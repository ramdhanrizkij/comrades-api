'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);

const Chat = Database.define(`chat`, {
  id_chat: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  id_user: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
  },
  id_usersahabat: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
  },
  status_pesanbaruuntukuser: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
  },
  status_pesanbaruuntuksahabat: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
  },
  pesanbaruuntukuser: {
    type: Sequelize.STRING,
    noteEmpty: true,
  },
  pesanbaruuntuksahabat: {
    type: Sequelize.STRING,
    noteEmpty: true,
  },
  tglpesanbaruuntukuser: {
    type: Sequelize.DATE,
    noteEmpty: true,
  },
  tglpesanbaruuntuksahabat: {
    type: Sequelize.DATE,
    noteEmpty: true,
  },
}, {
  timestamps: false,
  tableName: 'notif',
});
module.exports = Chat;
