'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);
const moment = require('moment');

const ReminderHistory = Database.define(`reminder_history`, {
  id_history: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  id_reminder: {
    type: Sequelize.INTEGER,
    validate: {
      notEmpty: true
    }
  },
  waktu_minum: {
    type: Sequelize.TIME,
    notEmpty: true,
    validate: {
      notEmpty: true
    }
  },
  tanggal_minum: {
    type: Sequelize.DATE,
    validate: {
      notEmpty: true
    }
  }
}, {
  timestamps: false,
  tableName: 'reminder_history',
});

ReminderHistory.beforeCreate(function (reminder, options) {
    reminder.set("waktu_minum", moment().format('H:m:s'));
    reminder.set("tanggal_minum", moment().format('YYYY-MM-DD'));
});

module.exports = ReminderHistory;
