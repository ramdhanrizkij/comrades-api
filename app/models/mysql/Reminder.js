'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);

const Reminder = Database.define(`reminder`, {
  id_reminder: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  id_obat: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
    validate: {
      notEmpty: true
    }
  },
  id_user: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
    validate: {
      notEmpty: true
    }
  },
  waktu_reminder: {
    type: Sequelize.ENUM,
    values: ['8', '12'],
    validate: {
      isIn: [['8', '12']],
    }
  },
  jam_mulai: {
    type: Sequelize.TIME,
    noteEmpty: true,
    validate: {
      notEmpty: true
    }
  }
}, {
  timestamps: false,
  tableName: 'reminderv2',
});

Reminder.beforeCreate(async (reminder, options) => {
    const getUserObat = await Reminder.findAll({
      where: {
        id_user: reminder.id_user,
        id_obat: reminder.id_obat
      }
    });

    if(getUserObat.length > 0)
      throw new Error("User sudah menyimpan reminder untuk obat ini.")
});

module.exports = Reminder;
