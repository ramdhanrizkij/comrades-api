'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);

const ChatFirebase = Database.define(`chatFirebase`, {
  id_chat: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  registration_token_from: {
    type: Sequelize.STRING
  },
  registration_token_to: {
    type: Sequelize.STRING
  },
  message: {
    type: Sequelize.STRING
  },
  time: {
    type: Sequelize.DATE
  }
}, {
  timestamps: false,
  tableName: 'chatFirebase',
});
module.exports = ChatFirebase;
