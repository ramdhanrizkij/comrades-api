'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);
const User = require(`./User`);

const SahabatOdha = Database.define(`sahabat_odha`, {
  id_sahabatodha: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  id_user: {
    type: Sequelize.INTEGER,
    foreignKey: true,
    notEmpty: true
  },
  komunitas: {
    type: Sequelize.STRING,
    notEmpty: true,
  },
  pekerjaan: {
    type: Sequelize.STRING,
    notEmpty: true,
  },
  institusi: {
    type: Sequelize.STRING,
    notEmpty: true,
  },
  usia: {
    type: Sequelize.INTEGER,
    notEmpty: true,
  },
  about_sahabatodha: {
    type: Sequelize.TEXT,
    notEmpty: true,
  },
  harga: {
    type: Sequelize.DOUBLE
  },
  asal_kota: {
    type: Sequelize.STRING
  },
  status_aktivasi: {
    type: Sequelize.ENUM,
    values: ['0', '1']
  },
}, {
  timestamps: false,
  tableName: 'sahabat_odha',
});

// SahabatOdha.belongsTo(User, {foreignKey: 'id_user', targetKey: 'id_user'});

module.exports = SahabatOdha;
