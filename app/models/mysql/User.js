'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);
const sahabatOdha = require(`./SahabatOdha`);
const security = require('../../services/security');

const User = Database.define(`user`, {
  id_user: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  nama: {
    type: Sequelize.TEXT,
    notEmpty: true,
    validate: {
      notEmpty: true
    }
  },
  email: {
    type: Sequelize.TEXT,
    notEmpty: true,
    validate: {
      isEmail: true,
      notEmpty: true,
      isUnique(value, next) {
        User.find({
          where: {email: security.encryptUser(value,'comrade@codelabs')},
          attributes: ['email']
        }).done((users) => {
          if (users)
              return next('Email already exist');
          next();
        });
      }
    }
  },
  password: {
    type: Sequelize.STRING,
    validate: {
      min: 6
    }
  },
  jk: {
    type: Sequelize.ENUM,
    values: ['L', 'P']
  },
  telp: {
    type: Sequelize.TEXT
  },
  tgl_lahir: {
    type: Sequelize.TEXT
  },
  status: {
    type: Sequelize.ENUM,
    values: ['0', '1'],
    validate: {
      notEmpty: true
    }
  },
  jenis_user: {
    type: Sequelize.ENUM,
    values: ['Odha', 'Odha + Sahabat Odha', 'Sahabat Odha', 'User'],
    validate: {
      notEmpty: true
    }
  },
  foto: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  private_key: {
    type: Sequelize.STRING,
    notEmpty: true,
  },
  registration_token_fcm: {
    type: Sequelize.TEXT,
    notEmpty: true,
    validate: {
      notEmpty: true
    }
  },
  firebase_token: {
    type: Sequelize.TEXT,
    notEmpty: true,
    validate: {
      notEmpty: true
    }
  },
}, {
  timestamps: false,
  tableName: 'user'
});

User.beforeCreate(function (user, options) {
    const key = security.generatePrivateKey();
    if (user.password) {
        user.set("password", security.hashPassword(10, user.password));
    } else {
        user.set("password", null);
    }

    user.set("nama", security.encryptUser(user.nama, key));
    user.set("email", security.encryptUser(user.email, 'comrade@codelabs'));

    if (user.telp) {
        user.set("telp", security.encryptUser(user.telp, key));
    } else {
        user.set("telp", null);
    }

    if (user.tgl_lahir) {
        user.set("tgl_lahir", security.encryptUser(user.tgl_lahir, key));
    } else {
        user.set("tgl_lahir", null);
    }

    user.set("jenis_user", 'User');
    user.set("private_key", key);
});

User.beforeUpdate(async function (user, options) {
    try {
      const dataUser = await User.findAll({
        attributes: ['private_key'],
        where: {
          id_user: user.id_user
        }
      });

      user.set("nama", security.encryptUser(user.nama, dataUser[0].private_key));
      // user.set("email", security.encryptUser(user.email, 'comrade@codelabs'));

      if (user.telp) {
          user.set("telp", security.encryptUser(user.telp, dataUser[0].private_key));
      }

      if (user.tgl_lahir) {
          user.set("tgl_lahir", security.encryptUser(user.tgl_lahir, dataUser[0].private_key));
      }

    } catch (e) {
      throw new Error(e)
    }

});


module.exports = User;
