'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);

const Obat = Database.define(`obat`, {
  id_obat: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  id_jenis_obat: {
    type: Sequelize.INTEGER,
    noteEmpty: true,
  },
  merek: {
    type: Sequelize.STRING,
    noteEmpty: true,
    validate: {
      notEmpty: true
    }
  },
  produsen: {
    type: Sequelize.STRING,
    noteEmpty: true,
  },
  kandungan: {
    type: Sequelize.STRING,
    noteEmpty: true,
  },
  bentuk: {
    type: Sequelize.STRING,
    noteEmpty: true,
  }
}, {
  timestamps: false,
  tableName: 'obat_arv',
});
module.exports = Obat;
