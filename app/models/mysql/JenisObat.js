'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);

const JenisObat = Database.define(`jenis_obat`, {
  id_jenis: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  nama: {
    type: Sequelize.STRING,
    noteEmpty: true,
  },
  note: {
    type: Sequelize.TEXT,
    noteEmpty: true,
  }
}, {
  timestamps: false,
  tableName: 'jenis_obat_arv',
});
module.exports = JenisObat;
