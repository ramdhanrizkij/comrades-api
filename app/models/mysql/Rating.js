'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../../../config/sequelize`);

const RatingSahabatOdha = Database.define(`rating`, {
  id_rating: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  id_pengerate: {
    type: Sequelize.INTEGER,
    foreignKey: true,
    notEmpty: true
  },
  id_user: {
    type: Sequelize.INTEGER,
    foreignKey: true,
    notEmpty: true
  },
  rating: {
    type: Sequelize.STRING,
    notEmpty: true
  },
  testimoni: {
    type: Sequelize.TEXT,
    notEmpty: true
  },
  tanggal: {
    type: Sequelize.DATE,
    notEmpty: true
  }
}, {
  timestamps: false,
  tableName: 'rating',
});

module.exports = RatingSahabatOdha;
