var mongoose = require('mongoose'),
		textSearch = require('mongoose-text-search'),
		Schema = mongoose.Schema;

var EventSchema = new Schema({
	pengirim: String,
	nama: String,
	slug: String,
	status: String,
	tempat: String,
	deskripsi: String,
	foto: String,
	tgl_posting: String,
	tgl_mulai: String,
	tgl_berakhir: String,
	longitude: Number,
	latitude: Number,
	lang: String,
	type: String,
	kontak_person: String,
    type:String,
    status:String
});

EventSchema.index({'$**': 'text'});

mongoose.model("event", EventSchema);
