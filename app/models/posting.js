var mongoose = require('mongoose'),
		textSearch = require('mongoose-text-search'),
		Schema = mongoose.Schema;

var PostingSchema = new Schema({
	kategori: String,
	pengirim: String,
	judul: {
		type: String,
		text: true
	},
	slug: String,
	deskripsi: String,
	isi: String,
	foto: String,
	status: String,
	tgl_posting: String,
	sumber: String,
	img_old: String,
	label: String,
	lang: String

});
// give our schema text search capabilities
// PostingSchema.plugin(textSearch);
// //PostingSchema.index({'$**': 'text'});
// PostingSchema.index({judul: 'text'});

mongoose.model("posting", PostingSchema);
