const bcrypt = require('bcryptjs');
const crypto = require('crypto');

module.exports = {

  hashPassword(saltRound, plainTextPassword) {
    const salt = bcrypt.genSaltSync(saltRound);

    return bcrypt.hashSync(plainTextPassword, salt);
  },

  encryptUser(text, key) {
    var cipher = crypto.createCipher('aes-128-ecb',key);
    var hasil = cipher.update(text.toString(),'utf-8','hex');
    hasil += cipher.final('hex');

    return hasil;
  },

  decryptUser(text, key) {
    var decipher = crypto.createDecipher('aes-128-ecb',key);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');

    return dec;
  },

  randomkey() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for( var i=0; i < 10; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
  },

  generatePrivateKey() {
    return this.randomkey();
  },

  comparePassword(password1, password2) {
    return bcrypt.compareSync(password1,password2 ? password2:'kosong')
  }

}
