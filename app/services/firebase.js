const firebase = require('../../config/firebase');
var firebaseDB = firebase.database();
var firebaseAuth = firebase.auth();

module.exports = {
  createTokenFirebase : async (req) => {
    try {
      var createUser = await firebaseAuth.createUserWithEmailAndPassword(req.body.email, req.body.password);

      return createUser;

    } catch (error) {

      return error;

    }

  }
}
