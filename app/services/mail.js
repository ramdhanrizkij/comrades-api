const transport = require('../../config/mail').transport;
const EmailTemplates = require('swig-email-templates');
const jwt = require('jsonwebtoken');

module.exports = {
  sendEmailConfirmation(req) {
    var token = jwt.sign(req.body, 'emailconfirmationcomrade', {
      expiresIn: "2h"
    });
    var templates = new EmailTemplates({root: 'app/views/emails'});
    var locals = {
        email: req.body.email,
        token: token,
        url: 'http://comrades-api.azurewebsites.net/confirm'
    };
    templates.render('confirm-email.html', locals, function(err, html) {
        if (err) {
          console.log(err);
        } else {
            transport.sendMail({
                from: 'Comrade app <Admin@comrade.com>',
                to: locals.email,
                subject: 'Email Confirmation.',
                html: html
                }, function(err, responseStatus) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(responseStatus);
                    }
                }
            );
        }
    });
  }
}
