var db = require('../../config/db');
var async = require('async');
var _ = require('lodash');
var moment = require('moment');
// var reminder = require('../../config/reminder');

function Todo() {

  this.obat = function(req,res,next) {
    db.acquire(function(err,con){
  		if (err) throw err;

      var find = req.query;
      var key = Object.keys(find).toString();
      var value = '';
      var vals = Object.keys(find).map(function(key) {
          value = find[key];
      });

      var query = '';

      if(key || value)
        query = `SELECT * FROM obat_arv WHERE ${key} = '${value}'`;
      else
        query = `SELECT * FROM obat_arv`;

  		con.query(query, function(err,data){
  			con.release();
  			if(err)
  				return res.json({status:500,message:err,result:[]});

  			return res.json({status:200,message:'success',result:data});

  		});
  	});
  };

  this.jenisTes = function(req,res,next) {
    db.acquire(function(err,con){
  		if (err) throw err;

      async.waterfall([
        (done) => {
            query = `SELECT id_jenis as id_jenis_obat,nama FROM jenis_obat_arv`;
            con.query(query, function(err,data){
        			if(err){
        			   done(err,null);
              }else
        			   done(null,data);
        		});
        },
        (JenisObat,done) => {
          con.query(`SELECT * FROM obat_arv`, function(err,data){
            if(err){
               done(err,null);
            }else{
              done(null,JenisObat,data);
            }
          });
        },
        (JenisObat,Obat,done) => {
          con.release();
          // console.log(JenisObat[0]);
          // console.log(Obat[0]);
          // var a = _.merge(JenisObat, Obat);
          // console.log(a);
          done(null,a);
        }
      ], (err,data) => {
          if(err)
            return res.json(err);
          else
            return res.json(data);
      });

  	});
  };

  this.jenis = (req,res) => {
    query = `SELECT * FROM jenis_obat_arv`;
    db.acquire(function(err,con){
      con.query(query, function(err,data){
        con.release();
        if(err)
           return res.json({status:500,message:err,result:[]});

           return res.json({status:200,message:'Success',result:data});
      });
    });
  }

  this.reminder = (req,res) => {
    var dataReminder = {
      id_healbox: 244,
      id_obat: req.body.id_obat,
      waktu_reminder: req.body.waktu_reminder,
      jangka_waktu: req.body.jangka_waktu,
      tanggal: moment().format('Y-m-d')
    };
    // var waktu_reminder = moment(dataReminder.waktu_reminder, 'HH:mm:ss').add(aturan_minum, 'hours').format('HH:mm:ss');

    if(req.body.slot == 1)
      dataReminder.data1 = 'ledHijau1';
    else if(req.body.slot == 2)
      dataReminder.data2 = 'ledHijau2';
    else
      dataReminder.data3 = 'ledHijau3';

    reminder.setReminder(dataReminder,(err,data) => {
      if(err)
        console.log(err);

      return res.json(data);
    });

  }

}

module.exports = new Todo();
