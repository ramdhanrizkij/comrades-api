var db = require('../../config/db');
const fs = require('fs');
var async = require('async');
var striptags = require('striptags');
var distance = require('google-distance');
var multer  = require('multer');
var geocoderProvider = 'google';
var httpAdapter = 'https';
var extra = {
    apiKey: 'AIzaSyDjE5MTfUt5RYaEdA_I_PVvaQJlkro5e80',
    formatter: null
};

var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter, extra); //menghasilkan address dari lat dan long

function Todo() {

this.simpanLokasi = function(req, res, next) {
    var message = null;
    var storage = multer.diskStorage({
       destination: function (req, file, callback) {
           callback(null, 'public/pic_lokasi');
       },
       filename: function (req, file, callback) {
           callback(null, Date.now() + '-' + file.originalname);
       }
   });
   var upload = multer({ storage : storage }).single('foto');
   upload(req,res,function(errupload) {
       var message = null;

       //var now = moment().format('DD MMMM YYYY');

       geocoder.geocode(req.body.posisi, function(err, result) {
         var data = {
              id_admin: req.body.id_admin,
              nama: req.body.nama,
              foto: req.file.filename,
              deskripsi: req.body.deskripsi,
              deskripsi_eng: req.body.deskripsi_en,
              open_timeinfo: req.body.waktu_mulai+' - '+req.body.waktu_berakhir,
              latitude: result[0].latitude,
              longitude: result[0].longitude,
			  status: "1"
            }
            db.acquire(function(err,con){
              con.query('INSERT INTO lokasi_obat SET ? ',data,function(err){
                con.release();
                //error simpan ke database
                if (err) {
                  return res.json({status:400,message:err.code,result:[]});
                }
                return res.json({status:200,message:'Success add lokasi obat'});
              });
            });
        });
    });
};

this.recommendLokasi_obat = function(req, res, next) {
    var message = null;
    var storage = multer.diskStorage({
       destination: function (req, file, callback) {
           callback(null, 'public/pic_lokasi');
       },
       filename: function (req, file, callback) {
           callback(null, Date.now() + '-' + file.originalname);
       }
   });
   var upload = multer({ storage : storage }).single('foto');
   upload(req,res,function(errupload) {
       var message = null;

       //var now = moment().format('DD MMMM YYYY');

       geocoder.geocode(req.body.posisi, function(err, result) {
         var data = {
              id_user: req.body.id_user,
              nama: req.body.nama,
              foto: req.file.filename,
              deskripsi: req.body.deskripsi,
              deskripsi_eng: req.body.deskripsi_en,
              open_timeinfo: req.body.waktu_mulai+' - '+req.body.waktu_berakhir,
              latitude: result[0].latitude,
              longitude: result[0].longitude,
			  status: "0"
            }
            db.acquire(function(err,con){
              con.query('INSERT INTO lokasi_obat SET ? ',data,function(err){
                con.release();
                if (err) {
                  return res.json({status:400,message:err.code,result:[]});
                }
                return res.json({status:200,message:'Success recommend lokasi obat'});
              });
            });
        });
    });
};


this.rekomendasi_obat = function(req, res, next) {
   var data = {
              nama: req.body.nama,
              foto: 'no-image.jpg',
              deskripsi: req.body.deskripsi,
              deskripsi_eng: req.body.deskripsi_en,
              open_timeinfo: req.body.waktu_mulai+' - '+req.body.waktu_berakhir,
              latitude: req.body.latitude,
              longitude: req.body.longitude,
			  status: "0"
            }
            db.acquire(function(err,con){
              con.query('INSERT INTO lokasi_obat SET ? ',data,function(err){
                con.release();
                if (err) {
                  return res.json({status:400,message:err.code,result:[]});
                }
                return res.json({status:200,message:'Success recommend lokasi obat'});
              });
            });
};

this.getRecommendLokasi_obat = function(req,res,next){
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('SELECT id_lokasi,lokasi_obat.nama,longitude,latitude,lokasi_obat.foto,deskripsi,deskripsi_eng,open_timeinfo FROM lokasi_obat WHERE lokasi_obat.status="0"', function(err,data){
			con.release();
			if(err){
                return res.json({status:400,message:err.code,result:[]});
            }else if(!data.length){
                return res.json({status:400,message: 'Data not found',result:[]})
            }
            return res.json({status:200,message:'success',result:data});
		});
	});
}

this.verifyRecommendLokasi_obat = function(req,res,next){
    var id = req.body.id;
    var data = {
        status : "1",
    }
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('UPDATE lokasi_obat SET ? WHERE id_lokasi='+id,data, function(err,data){
			con.release();
			if(err){
                return res.json({status:400,message:err.code,result:[]});
            }
            return res.json({status:200,message:'success verify location'});
		});
	});
}

this.lokasi_obat = function(req,res,next){
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('SELECT id_lokasi,id_admin,id_user,nama,longitude,latitude,foto,deskripsi,deskripsi_eng,open_timeinfo FROM lokasi_obat WHERE status="1"', function(err,data){
			con.release();
			if(err){
                return res.json({status:400,message:err.code,result:[]});
            }else if(!data.length){
                return res.json({status:400,message: 'Data not found',result:[]})
            }
            return res.json({status:200,message:'success',result:data});
		});
	});
}

this.lokasi_obatLineGet = function(req,res,next){
    db.acquire(function(err,con){
      con.query('SELECT id_lokasi,nama,longitude,latitude,foto,deskripsi,deskripsi_eng FROM lokasi_obat WHERE status="1"', function(err,data){
        con.release();
        if(err){
          return res.json({status:400,message:err.code,result:[]});
        }
        return res.json({status:200,message:'success',result:data});
      });
    });
}

this.lokasi_obatLine = function(req,res,next){
	db.acquire(function(err,con){
		if (err) throw err;
    var dataLokasi = [];
    var longitude = req.body.longitude;
    var latitude = req.body.latitude;
    var jarak = [];
    var dataFinal =[];
    async.parallel({
      one: function(done) {
          con.query('SELECT id_lokasi,nama,longitude,latitude,foto,deskripsi,deskripsi_eng FROM lokasi_obat WHERE status="1"', function(err,data){
            con.release();
            if(err)
                  return res.json({status:400,message:err.code,result:[]});
            if(!data.length)
                  return res.json({status:400,message: 'Data not found',result:[]})

            dataLokasi.push(data);

            for (var i = 0; i < dataLokasi[0].length; i++) {
              (function(i) {
                //desc
                var arrayisi = striptags(dataLokasi[0][i].deskripsi).split(' ');
                var sliceisi = arrayisi.slice(0,5);
                dataLokasi[0][i].deskripsi = sliceisi.join(' ')+'...';
                //nama lokasi
                var arrayisiTitle = striptags(dataLokasi[0][i].nama).split(' ');
                var sliceisiTitle = arrayisiTitle.slice(0,4);
                dataLokasi[0][i].nama = sliceisiTitle.join(' ');

                distance.get({
                  index: 1,
                  origin: ''+latitude+','+longitude+'',
                  destination: ''+dataLokasi[0][i].latitude+','+dataLokasi[0][i].longitude+''
                },function(err, data) {
                  if (err) return console.log(i+' Errors euyy '+err);
                  var datat = data.distance.replace(' km','');
                  jarak.push({no:i,jarak:data.distanceValue,latitude:dataLokasi[0][i].latitude});
                  // console.log(jarak);
                  dataLokasi[0][i].jarak = data.distanceValue;
                });
              })(i);
            }
            done(null,data);
         });
      }
    }, function (err) {
       if(err){
         console.log(err);
       }
       function log() {
         if(jarak.length > 0){
           clearInterval(logJarak);

           function SortByJarak(a, b){
              return a.jarak - b.jarak;
           }

           dataLokasi[0].sort(SortByJarak);

           for (var i = 0; i < dataLokasi[0].length; i++) {
             dataFinal.push({
                   thumbnailImageUrl: 'https://comrades-api.azurewebsites.net/pic_lokasi/'+dataLokasi[0][i].foto,
                   title: dataLokasi[0][i].nama,
                   text: dataLokasi[0][i].deskripsi,
                   actions: [
                       {
                           "type": "uri",
                           "label": "Petunjuk Arah",
                           "uri": "https://www.google.com/maps/dir/"+latitude+","+longitude+"/"+dataLokasi[0][i].latitude+","+dataLokasi[0][i].longitude+""
                       },
                       {
                           "type": "uri",
                           "label": "View detail",
                           "uri": "https://comrades-apps.azurewebsites.net/lokasi/"+dataLokasi[0][i].id_lokasi+""
                       }
                   ]
             });
             if (i == 4) {
               break;
             }
           }

            console.log("jarak TERISI");
            console.log(dataLokasi[0][1].jarak);
            return res.json({
              type:'template',
              altText:'Comrades Your Care For a Better Life',
              template: {
                type: 'carousel',
                columns:dataFinal
              }
            });
         }else{
            console.log("jarak belum TERISI");
            console.log(jarak.length);
         }
       }
       var logJarak = setInterval(log, 1000);
    });

	});
}
this.idlokasi_obat = function(req,res,next){
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('SELECT id_admin FROM lokasi_obat WHERE id_lokasi='+req.params.id, function(err,data){
			con.release();
			if(err){
                return res.json({status:400,message:err.code,result:[]});
            }else if(!data.length){
                return res.json({status:400,message: 'Data not found',result:[]})
            }
            if(data[0].id_admin == null){
                con.query('SELECT id_lokasi,lokasi_obat.nama,longitude,latitude,lokasi_obat.foto,deskripsi,deskripsi_eng,open_timeinfo FROM lokasi_obat WHERE id_lokasi='+req.params.id, function(err,data){
                    return res.json({status:200,message:'success',result:data});
                });
            }else{
                con.query('SELECT id_lokasi,admin.nama as pengirim,lokasi_obat.nama,longitude,latitude,lokasi_obat.foto,deskripsi,deskripsi_eng,open_timeinfo FROM lokasi_obat INNER JOIN admin ON admin.id_admin=lokasi_obat.id_admin WHERE id_lokasi='+req.params.id, function(err,data){
                    return res.json({status:200,message:'success',result:data});
                });
            }
		});
	});
}

this.deleteLokasi = function(req, res, next) {
    var id = req.body.id;
    db.acquire(function(err,con){
    con.query('SELECT * FROM lokasi_obat WHERE id_lokasi='+id,function(errselect,data){
      con.release();
        con.query('DELETE FROM lokasi_obat WHERE id_lokasi='+id,function(err){
            if(err)
                return res.json({status:400,message:err});

                data.forEach(function(data){
                    if(data.foto){
                        fs.unlink('public/pic_lokasi/'+data.foto,function(err){
                          if(err)
                            return res.json({status:200,message:'Success delete data'});

                        return res.json({status:200,message:'Success delete data'});
                        });
                    }else{
                      return res.json({status:200,message:'Success delete data'});
                    }
                });

        });
    });
    });
};

}

module.exports = new Todo();
