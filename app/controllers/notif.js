var db = require('../../config/db');
var Pusher = require('pusher');
var pusher = new Pusher({
  appId: '259913',
  key: '1bba48d795f7e899c4d0',
  secret: '0826f796c436807884b2'
});


function Todo() {
  this.chatListSahabatOdha = function(req, res, next) {
      db.acquire(function(err,con){
        var id = req.params.id;
        var limit = 8;
        var page = req.params.page;
        var offset = page;
        if (err) throw err;
          con.query('SELECT notif.id_chat,notif.id_user,user.nama as nama_user,user.foto as foto_user,notif.status_pesanbaruuntuksahabat as status_pesanbaru,pesanbaruuntuksahabat as pesan_baru,tglpesanbaruuntuksahabat as tglpesanbaru,user.private_key FROM notif INNER JOIN user on notif.id_user=user.id_user WHERE notif.id_usersahabat='+id+' ORDER BY tglpesanbaruuntuksahabat DESC LIMIT '+limit+' OFFSET '+offset, function(err,data){
            con.release();
              if(err){
                  return res.json({status:400,message:err.code,result:[]});
              }
              if(!data.length){
                  return res.json({status:404,message: 'Data not found'})
              }
              con.query('SELECT count(id_chat) as jml FROM notif WHERE notif.id_usersahabat='+id+' AND status_pesanbaruuntuksahabat="1"',function(err,data2){
                return res.json({status:200,message:'success',totalPesanBaru:data2[0].jml,result:data});
              });
          });
      });
  };
    this.chatListUser = function(req, res, next) {
        db.acquire(function(err,con){
          var id = req.params.id;
          var limit = 8;
          var page = req.params.page;
          var offset = page;
          // 1 = belum dibaca
          if (err) throw err;
            con.query('SELECT notif.id_chat,notif.id_usersahabat,user.nama as nama_user,user.foto as foto_user,notif.status_pesanbaruuntukuser as status_pesanbaru,pesanbaruuntukuser as pesan_baru,tglpesanbaruuntukuser as tglpesanbaru,user.private_key FROM notif INNER JOIN user on notif.id_usersahabat=user.id_user WHERE notif.id_user='+id+' ORDER BY tglpesanbaruuntukuser DESC LIMIT '+limit+' OFFSET '+offset, function(err,data){
              con.release();
                if(err){
                    return res.json({status:400,message:err.code,result:[]});
                }
                if(!data.length){
                    return res.json({status:404,message: 'Data not found'})
                }
                con.query('SELECT count(id_chat) as jml FROM notif WHERE notif.id_user='+id+' AND status_pesanbaruuntukuser="1"',function(err,data2){

                  return res.json({status:200,message:'success',totalPesanBaru:data2[0].jml,result:data});
                });
            });
        });
    };
  function cekUser(iduser,idsahabat,pesan) {
    db.acquire(function(err,con){
      con.query('SELECT * notif WHERE id_user='+iduser+' AND idsahabat='+idsahabat,function(err,data){
        con.release();
        if(data.length){
          var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
          var data = {
              status_pesanbaruuntuksahabat: '0',
              pesanbaruuntuksahabat: pesan,
              tglpesanbaruuntukuser: now
          }
          con.query('UPDATE notif SET ? where id_user='+iduser+' AND id_usersahabat='+idsahabat,data, function(err,data){
              if(err){
                  return res.json({status:400,message:err.code,result:[]});
              }
              return res.json({status:200,message:'success',result:data});
          });
        }else{
          return false;
        }
      });
    });
  }
  this.UserTambahPesan = function(req, res, next) {
      db.acquire(function(err,con){
        if (err) throw err;
        var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var dataq = {
             id_user: req.body.id_user,
             id_usersahabat: req.body.id_usersahabat,
             status_pesanbaruuntukuser: '0',
             status_pesanbaruuntuksahabat: '1',
             pesanbaruuntuksahabat: req.body.pesanbaruuntuksahabat,
             tglpesanbaruuntukuser: '',
             tglpesanbaruuntuksahabat: now
           }
           con.query('SELECT * FROM notif WHERE id_user='+dataq.id_user+' AND id_usersahabat='+dataq.id_usersahabat,function(err,data){
             con.release();
             console.log(data);
             if(data.length){
               var data = {
                   status_pesanbaruuntuksahabat: '1',
                   pesanbaruuntuksahabat: dataq.pesanbaruuntuksahabat,
                   tglpesanbaruuntukuser: now
               }
               con.query('UPDATE notif SET ? where id_user='+dataq.id_user+' AND id_usersahabat='+dataq.id_usersahabat,data, function(err,data){
                   if(err){
                       return res.json({status:400,message:err.code,result:[]});
                   }
                   return res.json({status:200,message:'success',result:data});
               });
             }else{
                con.query('INSERT INTO notif SET ? ',dataq, function(err,data){
                    if(err){
                        return res.json({status:400,message:err.code,result:[]});
                    }
                    // pusher.trigger('posting', 'chat', data ,function(error, req, res) {
                    //     console.log(error, req, res);
                    // });
                    return res.json({status:200,message:'success',result:[]});
                });
             }
           });
      });
  };

function cekSahabat(iduser,idsahabat,pesan) {
    con.query('SELECT * notif WHERE id_user='+iduser+' AND idsahabat='+idsahabat,function(err,data){
      con.release();
      if(data.length){
        var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var data = {
            status_pesanbaruuntuksahabat: '0',
            pesanbaruuntukuser: pesan,
            tglpesanbaruuntukuser: now
        }
        con.query('UPDATE notif SET ? where id_user='+iduser+' AND id_usersahabat='+idsahabat,data, function(err,data){
            if(err){
                return res.json({status:400,message:err.code,result:[]});
            }
            return res.json({status:200,message:'success',result:data});
        });
      }else{
        return false;
      }
    });
};
  this.SahabatTambahPesan = function(req, res, next) {
      db.acquire(function(err,con){
        if (err) throw err;
        var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var dataq = {
             id_user: req.body.id_user,
             id_usersahabat: req.body.id_usersahabat,
             status_pesanbaruuntukuser: '1',
             status_pesanbaruuntuksahabat: '0',
             pesanbaruuntukuser: req.body.pesanbaruuntukuser,
             tglpesanbaruuntukuser: now,
             tglpesanbaruuntuksahabat: ''
           }
           con.query('SELECT * FROM notif WHERE id_user='+dataq.id_user+' AND id_usersahabat='+dataq.id_usersahabat,function(err,data){
             con.release();
             console.log(data);
             if(data.length){
               var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
               var datab = {
                   status_pesanbaruuntukuser: '1',
                   pesanbaruuntukuser: dataq.pesanbaruuntukuser,
                   tglpesanbaruuntukuser: now
               }
               con.query('UPDATE notif SET ? where id_user='+dataq.id_user+' AND id_usersahabat='+dataq.id_usersahabat,datab, function(err,data){
                   if(err){
                       return res.json({status:400,message:err.code,result:[]});
                   }
                   return res.json({status:200,message:'success',result:data});
               });
             }else{
               con.query('INSERT INTO notif SET ? ',dataq, function(err,data){
                   if(err){
                       return res.json({status:400,message:err.code,result:[]});
                   }
                   return res.json({status:200,message:'success',result:data});
               });
             }
           });
      });
  };
  this.UpdatePesanSahabat = function(req, res, next) {
      db.acquire(function(err,con){
        if (err) throw err;
        var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var data = {
             id_chat: req.body.id_chat,
             pesanbaruuntukuser: req.body.pesanbaruuntukuser,
             status_pesanbaruuntuksahabat: '0',
             tglpesanbaruuntukuser: now
           }
          con.query('UPDATE notif SET ? where id_chat='+data.id_chat,data, function(err,data2){
            con.release();
              if(err){
                  return res.json({status:400,message:err.code,result:[]});
              }
              return res.json({status:200,message:'success',result:data2});
          });
      });
  };
  this.UpdatePesanUser = function(req, res, next) {
      db.acquire(function(err,con){
        if (err) throw err;
        var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var data = {
             id_chat: req.body.id_chat,
             pesanbaruuntuksahabat: req.body.pesanbaruuntuksahabat,
             status_pesanbaruuntukuser: '0',
             tglpesanbaruuntuksahabat: now
           }
          con.query('UPDATE notif SET ? where id_chat='+data.id_chat,data, function(err,data){
            con.release();
              if(err){
                  return res.json({status:400,message:err.code,result:[]});
              }
              return res.json({status:200,message:'success',result:data});
          });
      });
  };
}

module.exports = new Todo();
