var sequelize = require('sequelize');
var Obat = require(`../../models/mysql/Obat`);
var JenisObat = require(`../../models/mysql/JenisObat`);
var Reminder = require(`../../models/mysql/Reminder`);
var ReminderHistory = require(`../../models/mysql/ReminderHistory`);

Obat.belongsTo(JenisObat, {foreignKey: 'id_jenis_obat', targetKey: 'id_jenis'});
Reminder.belongsTo(Obat, {foreignKey: 'id_obat', targetKey: 'id_obat'});

module.exports = {
  listObat : async (req,res) => {
    var dataQuery = null;

    if(req.params.page) {
      const limit = 8;
      dataQuery = {
        attributes: {
          exclude: ['id_jenis_obat']
        },
        include: JenisObat,
        limit: limit,
        offset: (req.params.page-1)*limit
      }
    }else{
      dataQuery = {
        attributes: {
          exclude: ['id_jenis_obat']
        },
        include: JenisObat
      }
    }

    try {
      const obat = await Obat.findAll(dataQuery);

      res.json({status:200, message:'success', result:obat});
    }catch(error) {
      res.json({status:500,message:'error',result:error})
    }
  },

  saveObat: async (req,res) => {
    try{
      const saveObat = await Obat.create(req.body);

      res.json({status:200,message:'success',result:saveObat});
    }catch(error) {
      res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }
  },

  listJeniObat: async (req,res) => {
    try {
      const obat = await JenisObat.findAll();

      res.json({status:200, message:'success', result:obat});
    }catch(error) {
      res.json({status:500,message:'error',result:error})
    }
  },

  setReminder: async (req,res) => {
    try {
      const saveReminder = await Reminder.create(req.body);

      res.json({status:200,message:'success',result:saveReminder});
    } catch (error) {
      res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }
  },

  editReminder: async (req,res) => {
    try {
      const updateReminder = await Reminder.update({
        id_obat:req.body.id_obat,
        waktu_reminder:req.body.waktu_reminder,
        jam_mulai:req.body.jam_mulai
      }, {
        where : {
          id_reminder: req.body.id_reminder
        }
      });

      res.json({status:200,message:'success',result:updateReminder});
    } catch (error) {
      res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }

  },

  deleteReminder: async (req,res) => {
    try {
        const deleteReminder = await Reminder.destroy({
          where: {
              id_reminder: req.body.id_reminder
          }
        });

        if(deleteReminder == 0) {
          res.status(400).json(
            {
              status: 400,
              message: 'ID Reminder not found',
              result: []
            }
          );
        }
        res.json({status:200,message:'success',result:[]});
    } catch (error) {
        res.status(400).json(
          {
            status: 400,
            message: error.message,
            result: error.errors ? error.errors : []
          }
        );
    }
  },

  getUserReminder: async (req,res) => {
    try {
      const saveReminder = await Reminder.findAll({
        where: {
          id_user: req.params.id_user
        },
        include: Obat,
        attributes: {
          exclude: ['id_obat']
        }
      });

      res.json({status:200,message:'success',result:saveReminder});
    } catch (error) {
      res.json({status:500,message:'error',result:error})
    }
  },

  minumObat: async (req,res) => {
    try {
      const setReminderHistory = await ReminderHistory.create(req.body);
      res.json({status:200,message:'success',result:setReminderHistory});
    } catch (error) {
      res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }
  }
}
