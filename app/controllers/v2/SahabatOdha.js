var sequelize = require('sequelize');
var SahabatOdha = require(`../../models/mysql/SahabatOdha`);
var User = require(`../../models/mysql/User`);
var Rating = require(`../../models/mysql/Rating`);
var AES = require('../AES');

User.belongsTo(SahabatOdha, {foreignKey: 'id_user', targetKey: 'id_user'});
User.hasMany(Rating, {foreignKey: 'id_user', targetKey: 'id_user'});

module.exports = {
  list : async (req,res) => {
    try {
      var dataSahabatOdha = await User.findAll({
        attributes: {
          exclude: ['password']
        },
        where: {
          jenis_user: 'Sahabat Odha'
        },
        include: [
          {
            model: SahabatOdha
          },
          {
            model: Rating,
            attributes: [
              [sequelize.fn('sum', sequelize.col('rating')), 'rating'],
              [sequelize.fn('count', sequelize.col('id_rating')), 'count'],
            ],
          }
        ],
        group: ['id_user']
      });

      for (i in dataSahabatOdha) {
        if(dataSahabatOdha[i].ratings.length != 0) {
          var resultSahabatOdha = JSON.parse(JSON.stringify(dataSahabatOdha[i].ratings[0]));
          dataSahabatOdha[i].ratings[0].rating = resultSahabatOdha.rating / resultSahabatOdha.count;
        }
      }

      res.json({status:200,message:'success',result:dataSahabatOdha});
    }catch(error) {
      res.json({status:500,message:'error',result:error})
    }

  },

  listOdha : async (req,res) => {
    try {
      const dataOdha = await User.findAll({
        attributes: {
          exclude: ['password']
        },
        where: {
          jenis_user: 'Odha'
        }
      });

      for(var data in dataOdha) {
        dataOdha[data].nama = AES.decrypt(dataOdha[data].nama,dataOdha[data].private_key);
        dataOdha[data].email = AES.decrypt(dataOdha[data].email,dataOdha[data].private_key);
        dataOdha[data].telp = AES.decrypt(dataOdha[data].telp,dataOdha[data].private_key);
        dataOdha[data].tgl_lahir = AES.decrypt(dataOdha[data].tgl_lahir,dataOdha[data].private_key);
      }

      res.json({status:200,message:'success',result:dataOdha});
    }catch(error) {
      res.json({status:500,message:'error',result:error})
    }
  }
}
