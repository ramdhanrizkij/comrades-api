const Chat = require(`../../models/mysql/Chat`);
const SahabatOdha = require(`../../models/mysql/SahabatOdha`);
const User = require(`../../models/mysql/User`);
const ChatFirebase = require(`../../models/mysql/ChatFirebase`);
const firebase = require('../../../config/firebase');
const firebaseAdmin = require('../../../config/firebaseAdmin');
const moment = require('moment');
var firebaseDB = firebase.database();
var firebaseAuth = firebase.auth();

Chat.belongsTo(User, {as: 'User', foreignKey: 'id_user', targetKey: 'id_user'});
Chat.belongsTo(User, {as: 'Sahabat', foreignKey: 'id_usersahabat', targetKey: 'id_user'});
ChatFirebase.belongsTo(User, {as: 'UserFrom', foreignKey: 'registration_token_from', targetKey: 'registration_token_fcm'});
ChatFirebase.belongsTo(User, {as: 'UserTo', foreignKey: 'registration_token_to', targetKey: 'registration_token_fcm'});

module.exports = {
  updateRegistrationToken : async (req,res) => {
    try {
      const updateUser = await User.update({
        registration_token_fcm:req.body.registration_token_fcm,
      }, {
        where : {
          id_user: req.body.id_user
        }
      });

      if(updateUser[0] == 0) {
        res.json({status:200,message:'tidak ada perubahan',result:updateUser[0]});
      }

      res.json({status:200,message:'success',result:updateUser[0]});
    } catch (error) {
      res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }
  },

  getChat : async (req,res) => {
    try {
      var dataUser = await User.findOne({
        where: {
          uid: req.body.uid
        }
      });
    }catch(error) {
      res.json({status:500,message:'something went wrong',result:error});
    }
    if(dataUser != null) {
      if(dataUser.jenis_user == 'Sahabat Odha') {
        dataChat = {
          attributes: {
            exclude: ['status_pesanbaruuntukuser','status_pesanbaruuntuksahabat','pesanbaruuntukuser','tglpesanbaruuntukuser','id_user']
          },
          where: {
            id_usersahabat: dataUser.id_user
          },
          include: [{
            model: User,
            as: 'User',
            attributes: {
              exclude: ['password']
            }
          }],
          order: [
            ['tglpesanbaruuntuksahabat','DESC']
          ]
        };
      }else if(dataUser.jenis_user == 'User'){
        dataChat = {
          attributes: {
            exclude: ['status_pesanbaruuntukuser','status_pesanbaruuntuksahabat','pesanbaruuntuksahabat','tglpesanbaruuntuksahabat','id_usersahabat']
          },
          where: {
            id_user: dataUser.id_user
          },
          include: [{
            model: User,
            as: 'Sahabat',
            attributes: {
              exclude: ['password']
            }
          }],
          order: [
            ['tglpesanbaruuntukuser','DESC']
          ]
        };
      }

      try {
        const result = await Chat.findAll(dataChat);
        res.json({status:200,message:'success',result:result});
      } catch (e) {
        console.log(e);
        res.json({status:500,message:'something went wrong',result:e});
      }
    }else{
      res.json({status:401,message:'user not found',result:{}});
    }
  },

  simpanChat : async (tokenRecevier,tokenSender,message) => {
    try {
      var tokenRecevier = await User.findOne({
        where: {
          uid: tokenRecevier
        }
      });

      var tokenSender = await User.findOne({
        where: {
          uid: tokenSender
        }
      });

      if(tokenRecevier.jenis_user == 'Sahabat Odha') {
        var chatSimpan = {
          id_user : tokenSender.id_user,
          id_usersahabat : tokenRecevier.id_user,
        }
      }

    }catch(error) {
      res.json({status:500,message:'something went wrong',result:error});
    }
    // Chat.create({ title: 'foo', description: 'bar', deadline: new Date() }).then(task => {
    //   // you can now access the newly created task via the variable task
    // })
  },

  getKonsultasiFromFirebase : async (req,res) => {
    var data = [];
    var getChat = await firebaseDB.ref(`/${req.body.uid}`).once('value');

    getChat.forEach(result => {
      if(req.body.uidSender) {
        console.log(result.uid);
        if(result.uid == req.body.uidSender)
          data.push(result);
      }else{
        data.push(result);
      }
    });

    res.json({status:200,message:'success',result:data});

  },

  konsultasi : async (req,res) => {
    const uidRecevier = req.body.uidRecevier;
    const uidSender = req.body.uidSender;
    const message = req.body.message;

    try {
      var ref = firebaseDB.ref(`/${uidRecevier}`);
      var obj = {
        uid: uidSender,
        message: message,
        status: 0,
        date: moment().format('YYYY-MM-DD'),
        time: moment().format('h:mm:ss'),
      };

      ref.push(obj);

      // this.simpanChat(tokenRecevier,tokenSender,message);

      res.json({status:200,message:'success',result:obj});

    }catch(error) {
      console.log(error);
    }
  },

  async simpanChatFromFirebase(data) {
    try {
      const saveChat = await ChatFirebase.create(data);

      res.json({status:200, message:'success', result:saveChat});
    } catch (error) {
      res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }
  },

  konsultasiSendMessage: async (req,res) => {
    const data = {
      registration_token_to: req.body.registrationTokenTo,
      registration_token_from: req.body.registrationTokenFrom,
      message: req.body.message,
      time: `${moment().format('YYYY-MM-DD')} ${moment().format('h:mm:ss')}`
    }

    try {
      const findTokenFcm = await User.findOne({
        where: {
          registration_token_fcm: data.registration_token_to
        }
      });

      if(!findTokenFcm) {
        return res.status(400).json(
          {
            status: 400,
            message: 'Wrong Registration Token',
            result: {}
          }
        );
      }
    } catch (error) {
      return res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error
        }
      );
    }

    var message = {
      data: {
        message: req.body.message
      },
      token: data.registration_token_to
    };

    firebaseAdmin.messaging().send(message)
      .then((response) => {
        console.log(response);
        ChatFirebase.create(data).then(data => {
          return res.json({status:200,message:'success',result:data});
        });
      })
      .catch((error) => {
        return res.status(400).json(
          {
            status: 400,
            message: error.message,
            result: error
          }
        );
      });
  },

  konsultasiGetMessage: async (req,res) => {
    const user1 = req.body.user1;
    const user2 = req.body.user2;

    const user = await User.findAll({
      where: {
        $or: [
          {
            id_user: {
              $eq: user1
            },
          },
          {
            id_user: {
              $eq: user2
            }
          }
        ]
      }
    });

    if(user.length < 2) {
      return res.status(400).json(
        {
          status: 400,
          message: 'user1 or user2 not found',
          result: {}
        }
      );
    }

    if(!user[0].registration_token_fcm) {
      return res.status(400).json(
        {
          status: 400,
          message: 'registration token user1 or user2 not found',
          result: {}
        }
      );
    }

    if(!user[1].registration_token_fcm) {
      return res.status(400).json(
        {
          status: 400,
          message: 'registration token user1 or user2 not found',
          result: {}
        }
      );
    }

    const chatFirebase = await ChatFirebase.findAll({
      where: {
        $or: [
          {
            $and: [
              {
                registration_token_from: {
                  $eq: user[0].registration_token_fcm
                },
              },
              {
                registration_token_to: {
                  $eq: user[1].registration_token_fcm
                }
              }
            ]
          },
          {
            $and: [
              {
                registration_token_from: {
                  $eq: user[1].registration_token_fcm
                },
              },
              {
                registration_token_to: {
                  $eq: user[0].registration_token_fcm
                }
              }
            ]
          }
        ],
      },
      order: [
        ['time', 'ASC']
      ]
    });

    if(chatFirebase.length == 0) {
      return res.status(400).json(
        {
          status: 400,
          message: 'user message not found',
          result: {}
        }
      );
    }

    return res.json({
      status:200,
      message:'success',
      result:  {
        user1: {
          id_user: user[0].id_user,
          registration_token_fcm: user[0].registration_token_fcm,
          nama: user[0].nama,
          foto: user[0].foto
        },
        user2: {
          id_user: user[1].id_user,
          registration_token_fcm: user[1].registration_token_fcm,
          nama: user[1].nama,
          foto: user[1].foto
        },

        listChat: chatFirebase
      }
    });
  },

  sahabatodhaListChatKonsultasi: async (req,res) => {
    const user = await User.findOne({
      where: {
        id_user: req.params.id,
        jenis_user: 'Sahabat Odha'
      }
    });

    if(!user) {
      return res.status(400).json(
        {
          status: 400,
          message: 'user not found',
          result: {}
        }
      );
    }

    if(!user.registration_token_fcm) {
      return res.status(400).json(
        {
          status: 400,
          message: 'registration token not found',
          result: {}
        }
      );
    }

    const chatFirebase = await ChatFirebase.findAll({
      attributes: {
        exclude: ['registration_token_from']
      },
      include: [{
        model: User,
        as: 'UserTo',
        attributes: {
          exclude: ['password','firebase_token','private_key','status']
        }
      }],
      group: ['id_user'],
      where: {
        registration_token_from: user.registration_token_fcm
      }
    });

    if(chatFirebase.length == 0) {
      return res.status(400).json(
        {
          status: 400,
          message: 'user message not found',
          result: {}
        }
      );
    }

    return res.json({
      status:200,
      message:'success',
      result:  {
        user: {
          id_user: user.id_user,
          registration_token_fcm: user.registration_token_fcm,
          nama: user.nama,
          foto: user.foto
        },

        listChatSahabatOdha: chatFirebase
      }
    });

  }
}
