const User = require(`../../models/mysql/User`);
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const db 	= require('../../../config/db');
const AES = require('../AES');
const Service = require('../../services/security');
const mail = require('../../services/mail');
const Firebase = require('../../services/firebase');
const multer = require('multer');
const fs = require('fs');

module.exports = {
  decryptUser : async (req,res) => {
    var email = req.body.email;
    // console.log(bcrypt.hashSync(email, bcrypt.genSaltSync(8), null));
    try {
      var dataUser = await User.findOne({
        where: {
          email: email
        }
      });
      dataUser.email = AES.decrypt(dataUser.email,'comrade@codelabs');
      dataUser.nama = AES.decrypt(dataUser.nama,dataUser.private_key)
      if(dataUser.telp)
        dataUser.telp = AES.decrypt(dataUser.telp,dataUser.private_key);
      if(dataUser.tgl_lahir)
        dataUser.tgl_lahir = AES.decrypt(dataUser.tgl_lahir,dataUser.private_key);
      delete dataUser.password;
      res.json({status:200,message:'success',result:dataUser});
    }catch(error) {
      res.json({status:500,message:'error',result:error})
    }
  },

  encryptUser : async (req,res) => {

  },

  user: async (req,res) => {
    try {
      const dataUser = await User.findAll({
        attributes: {
          exclude: ['password']
        },
        where: {
          jenis_user: 'User'
        }
      });

      for(var data in dataUser) {
        dataUser[data].nama = AES.decrypt(dataUser[data].nama,dataUser[data].private_key);
        dataUser[data].email = AES.decrypt(dataUser[data].email,"comrade@codelabs");
        dataUser[data].telp = AES.decrypt(dataUser[data].telp,dataUser[data].private_key);
        dataUser[data].tgl_lahir = AES.decrypt(dataUser[data].tgl_lahir,dataUser[data].private_key);
      }

      res.json({status:200,message:'success',result:dataUser});
    }catch(error) {
      res.json({status:500,message:'error',result:error})
    }
  },

  getOneUser: async (req,res) => {

  },

  loginv2: async (req,res) => {
    try {
      const user = await User.find({
        where: {
          email: Service.encryptUser(req.body.email, 'comrade@codelabs')
        }
      });

      var userResult = null;
      if(user != null) {
        userResult= JSON.parse(JSON.stringify(user));
        var token = jwt.sign(userResult, 'comradeapp');
      }

    } catch (error) {
      return res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors ? error.errors : []
        }
      );
    }

    if(req.body.typeLogin == 'google') {
      if(userResult) {
        delete userResult.password;

        if(req.body.registration_token_fcm) {
          await User.update({
            registration_token_fcm: req.body.registration_token_fcm
          }, {
            where : {
              id_user: userResult.id_user
            }
          });

          const user = await User.find({
            where: {
              email: Service.encryptUser(req.body.email, 'comrade@codelabs')
            }
          });

          userResult= JSON.parse(JSON.stringify(user));
        }

        return res.json({
            status: 200,
            message: 'Success',
            token: token,
            result: userResult
        });
      }else{
        var googleRegistrationData = {
            email: req.body.email,
            password: null,
            nama: `${req.body.nama}`,
            foto: req.body.image_profile,
            jk: null,
            tgl_lahir: null,
            status: '1',
            jenis_user: 'User',
            private_key: Service.generatePrivateKey(),
            registration_token_fcm: req.body.registration_token_fcm,
            uid: null
        }
        const token = jwt.sign(googleRegistrationData, 'comradeapp');
        try {
          const simpanGoogleRegistrationData = await User.create(googleRegistrationData);
          const getGoogleRegistrationData = await User.findAll({
            where: {
              email : Service.encryptUser(googleRegistrationData.email, 'comrade@codelabs')
            }
          });
          return res.json({
              status: 200,
              message: 'Success',
              token: token,
              result: getGoogleRegistrationData[0]
          });
        } catch (error) {
          res.status(400).json(
            {
              status: 400,
              message: error,
              result: {}
            }
          );
        }
      }

    }else{
      if(userResult == null) {
        return res.status(401).json(
          {
            status: 401,
            message: 'Authentication failed. Email not found.',
            result: {}
          }
        );
      }

      if(userResult.status == '0') {
        return res.status(401).json(
          {
            status: 401,
            message: 'Authentication failed. Email has not been confirmed.',
            result: {}
          }
        );
      }

      const cekPassword = Service.comparePassword(req.body.password, userResult.password);
      if(!cekPassword) {
        return res.status(401).json(
          {
            status: 401,
            message: 'Authentication failed. Invalid Password.',
            result: {}
          }
        );
      }

      delete userResult.password;

      if(req.body.registration_token_fcm) {
        await User.update({
          registration_token_fcm: req.body.registration_token_fcm
        }, {
          where : {
            id_user: userResult.id_user
          }
        });

        const user = await User.find({
          where: {
            email: Service.encryptUser(req.body.email, 'comrade@codelabs')
          }
        });

        userResult= JSON.parse(JSON.stringify(user));
      }

      return res.json({
          status: 200,
          message: 'Success',
          token: token,
          result: userResult
      });
    }

  },

  login : async (req,res) => {
    req.checkBody("email", "Enter a valid email address.").isEmail();
  	var errors = req.validationErrors();
    	if (errors) {
    	  return res.send({
    	  	result: 'Failed',
    	  	status: 400,
    	  	errors: errors
    	  });
    	} else {
    		db.acquire(function(err,con){
  				if (err) throw err;
  				var email = req.body.email;
  			  con.query('SELECT * FROM user WHERE email="'+AES.encrypt(email,'comrade@codelabs')+'"', function(err,data){
    				con.release();
          	if(!data.length){
    					return res.json({ status:401, message: 'Authentication failed. Email not found.', result:{} });
    				}else if(data){

              if(data[0].status == '0'){
                return res.json({message:'Maaf email yang anda masukan belum dikonfirmasi.',status:401, result:{}})
              }

              var validPassword = bcrypt.compareSync(req.body.password,data[0].password);
					    if(!validPassword){
					    	return res.json({status:401, message: 'Authentication failed. Wrong password.', result:{} });
					    }
              var token = jwt.sign(data[0], 'comradeapp', {
                //expiresIn: "24h" // expires in 24 hour
              });
              var data = {
                id_user: data[0].id_user,
                nama: data[0].nama,
                email: data[0].email,
                jenis_kelamin: data[0].jk,
                foto: data[0].foto,
                telepon: data[0].telp,
                jenis_user:data[0].jenis_user,
                tgl_lahir:data[0].tgl_lahir,
                private_key:data[0].private_key,
                token_fcm:data[0].token_fcm,
                uid:data[0].uid
              }

              return res.json({
                  status: 200,
                  message: 'Success',
                  token: token,
                  result: data
              });
            }
  		    });
		    });
	   }
	},

  register : async (req,res) => {
    try {
      var createTokenFirebase = await Firebase.createTokenFirebase(req);
      if(createTokenFirebase.message) {
        return res.status(400).json(
          {
            status: 400,
            message: createTokenFirebase.message,
            result: {}
          }
        );
      }
      req.body.firebase_token = JSON.stringify(createTokenFirebase.user);
      req.body.status = "0";
      var simpanUser = await User.create(req.body);
      simpanUser = JSON.parse(JSON.stringify(simpanUser));
      delete simpanUser.firebase_token;

      var simpanUserResult = {
        User : simpanUser,
        firebase : JSON.parse(JSON.stringify(createTokenFirebase.user)).stsTokenManager
      }

      mail.sendEmailConfirmation(req);
      
      return res.json({status:200,message:'success',result:simpanUserResult});
    } catch (error) {

      return res.status(400).json(
        {
          status: 400,
          message: error.message,
          result: error.errors
        }
      );
    }
  },

  editUserFoto: async (req,res) => {
    const storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, 'public/pic_user');
        },
        filename: function (req, file, callback) {
            callback(null, Date.now() + '-' + file.originalname);
        }
    });

    var upload = multer({ storage : storage }).single('foto');

    try {
      const result = await new Promise((resolve,reject) => {
        resultUpload = upload(req,res, async (error) => {
          if(error) {
            reject(error);
          }

          const dataUser = await User.findAll({
            where : {
              id_user : req.body.id_user
            }
          });

          if(dataUser.length == 0) {
            reject('User not found');
          }

          if(!req.file) {
            reject('Foto not found');
          }

          req.dataUser = dataUser;
          resolve(req);

        });
      });

      if((result.dataUser[0].foto) && (result.dataUser[0].foto != 'default.png') ) {
        fs.unlink('public/pic_user/'+result.dataUser[0].foto);
      }

      try {
        const updateUser = await User.update({
          foto:result.file.filename
        }, {
          where : {
            id_user: result.dataUser[0].id_user
          }
        });

        return res.json(
          {
            status: 200,
            message: 'Success',
            result: {
              id_user: result.dataUser[0].id_user,
              foto: result.dataUser[0].foto
            }
          }
        );
      } catch (error) {
        return res.status(400).json(
          {
            status: 400,
            message: 'failed',
            result: error
          }
        );
      }


    } catch (error) {
      return res.status(400).json(
        {
          status: 400,
          message: 'failed',
          result: error
        }
      );
    }
  },

  editUser: async (req,res) => {
    try {
      var user = await User.find({
        where: {
          id_user: req.body.id_user
        }
      });

      if(user === null){
        return res.status(400).json(
          {
            status: 400,
            message: 'User not found',
            result: {}
          }
        );
      }

      user.nama = req.body.nama;
      user.jk = req.body.jenis_kelamin;
      if(req.body.telp)
        user.telp = req.body.telp;
      if(req.body.tgl_lahir)
        user.tgl_lahir = req.body.tgl_lahir;

      await user.save();

      user = JSON.parse(JSON.stringify(user));
      delete user.password;
      delete user.firebase_token;
      delete user.private_key;
      delete user.jenis_user;
      delete user.status;
      delete user.registration_token_fcm;

      return res.json(
        {
          status: 200,
          message: 'Success',
          result: user
        }
      );
    } catch (error) {
      return res.status(400).json(
        {
          status: 400,
          message: 'failed',
          result: error
        }
      );
    }
  }
}
