var db = require('../../config/db');
var async = require('async');

function Todo() {

this.TambahIdHealbox = function(req,res,next){
	db.acquire(function(err,con){
		if (err) throw err;
		var data = {
			id_healbox : req.body.id_healbox
		}
		con.query('INSERT INTO healbox SET ?',data, function(err,data){
			con.release();
				if(err)
          return res.json({status:400,message:err.code,result:[]});

        return res.json({status:200,message:'success add id healbox'});
		});
	});
}

this.reminder = function(req,res,next){
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('SELECT * FROM arv_reminder', function(err,data){
			con.release();
			if(err){
                return res.json({status:400,message:err.code,result:[]});
            }else if(!data.length){
                return res.json({status:404,message: 'Data not found',result:'Failed'})
            }
            return res.json({status:200,message:'success',result:data});
		});
	});
}

this.reminderPost = function (req,res,next) {
	 db.acquire(function(err,con){

	 });
};

this.updateID = function (req,res,next) {
	var datau = {
			id_user : req.body.id_user
	}
	var id = req.body.id_healbox;
	var cekId = false;
	db.acquire(function(err,con){
		if (err) throw err;
		async.parallel({
			one: function(done) {
							con.query('SELECT id_user FROM healbox where id_healbox='+id, function(err,data){
								con.release();
								if(err)
									return res.json({status:400,message:err.code,result:[]});
								if(!data.length)
									return res.json({status:404,message:'ID Healbox not found',result:[]});

								if(data[0].id_user){
									done(null,true);
								}else{
									cekId = true;
									done(null,false);
								}
							});
			}
		}, function (err) {
			  console.log(Boolean(cekId));
			  if(Boolean(cekId)){
					con.query('UPDATE healbox SET ? WHERE id_healbox='+id,datau, function(err,data){
						 if(err)
							return res.json({status:400,message:err.code,result:[]});

							return res.json({status:200,message:'success update id user'});
					});
				}else{
					return res.json({status:400,message:'id healbox already taken'});
				}

			});
	});
};

this.arv_reminder = function (req,res,next) {
	var data = {
			id : req.body.id,
			id_healbox : req.body.id_healbox,
			idarvtype : req.body.idarvType,
			tipearv : req.body.arvType,
			warnaLabel : req.body.warnaLabel,
			kategori_jam : req.body.kategoriJam,
			starthour : req.body.startHour,
			startminute : req.body.startMinute,
			totalDetikStart : req.body.totalDetikStart,
			idPiStart : req.body.idPiStart,
			middlehour : req.body.middleHour,
			middleminute : req.body.middleMinute,
			totalDetikMiddle : req.body.totalDetikMiddle,
			idPiMiddle : req.body.idPiMiddle,
			endhour : req.body.endHour,
			endminute : req.body.endMinute,
			totalDetikEnd : req.body.totalDetikEnd,
			idPiEnd : req.body.idPiEnd
	}
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('INSERT INTO arv_reminder SET ?',data, function(err,data){
			 con.release();
			 if(err)
				return res.json({status:400,message:err.code,result:[]});

				return res.json({status:200,message:'success insert new reminder arv'});
		});
	});
};

this.put_arv_reminder = function (req,res,next) {
	var data = {
			id_healbox : req.body.id_healbox,
			idarvtype : req.body.idarvtype,
			tipearv : req.body.tipearv,
			warnaLabel : req.body.warnaLabel,
			kategori_jam : req.body.kategori_jam,
			starthour : req.body.starthour,
			startminute : req.body.startminute,
			totalDetikStart : req.body.totalDetikStart,
			idPiStart : req.body.idPiStart,
			middlehour : req.body.middlehour,
			middleminute : req.body.middleminute,
			totalDetikMiddle : req.body.totalDetikMiddle,
			idPiMiddle : req.body.idPiMiddle,
			endhour : req.body.endhour,
			endminute : req.body.endminute,
			totalDetikEnd : req.body.totalDetikEnd,
			idPiEnd : req.body.idPiEnd
	}
	var id = req.params.id;
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('UPDATE arv_reminder SET ? WHERE id='+id,data, function(err,data){
			 con.release();
			 if(err)
				return res.json({status:400,message:err.code,result:[]});
			 if(!data.affectedRows)
 			 	return res.json({status:404,message:'ID not found',result:[]});

				return res.json({status:200,message:'success update reminder arv'});
		});
	});
};

this.healboxID = function(req,res,next) {
	var id = req.params.id_user;
	db.acquire(function(err,con){
		if (err) throw err;
		con.query('SELECT id_healbox FROM healbox WHERE id_user='+id, function(err,data){
			 con.release();
			 if(err)
				return res.json({status:400,message:err.code,result:[]});
			 if(!data.length)
 			 	return res.json({status:404,message:'ID not found',result:[]});

				return res.json({status:200,message:'success',result:data});
		});
	});
}

this.del_arv_reminder = function(req, res, next) {
    var id = req.params.id;
    db.acquire(function(err,con){

			con.query('DELETE FROM arv_reminder WHERE id='+id,function(err,data){
				con.release();
				if(err)
					return res.json({status:400,message:err});

				if(!data.affectedRows)
					return res.json({status:400,message:'id not found'});

				return res.json({status:200,message:'Success delete data'});

			});

    });
};

}

module.exports = new Todo();
