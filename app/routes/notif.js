var notif = require('../controllers/notif');

module.exports = {
  configure: function(app) {
    app.route('/UserTambahPesan').post(notif.UserTambahPesan);
    app.route('/SahabatTambahPesan').post(notif.SahabatTambahPesan);
    app.route('/SahabatUpdatePesan').post(notif.UpdatePesanSahabat);
    app.route('/UserUpdatePesan').post(notif.UpdatePesanUser);
    app.route('/chatListSahabat/:id/page/:page').get(notif.chatListSahabatOdha);
    app.route('/chatListUser/:id/page/:page').get(notif.chatListUser);
  }
};
