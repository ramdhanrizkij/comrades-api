var lokasi = require('../controllers/lokasi_obat');
var jwt = require('jsonwebtoken');
var cek = require('../../config/cektoken');

module.exports = {
  configure: function(app) {
    app.route('/lokasi_obat').get(lokasi.lokasi_obat).post(lokasi.simpanLokasi);

    app.route('/lokasi_obatLine').post(lokasi.lokasi_obatLine).get(lokasi.lokasi_obatLineGet);

    app.route('/lokasi_obatLine/:id').get(lokasi.lokasi_obatLineGet);

    app.route('/recommendLokasi_obat').post(lokasi.recommendLokasi_obat).get(lokasi.getRecommendLokasi_obat);

    app.route('/rekomendasilokasi').post(lokasi.rekomendasi_obat);

    app.route('/verifyRecommendLokasi_obat').post(lokasi.verifyRecommendLokasi_obat);

    app.route('/lokasi_obat/:id').get(lokasi.idlokasi_obat);

    app.route('/delLokasi_obat').post(lokasi.deleteLokasi);
  }
};
