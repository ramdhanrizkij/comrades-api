var express = require('express');
var router = express.Router();
var topikController = require('../../controllers/v2/topik');
var chatController = require('../../controllers/v2/chat');
var sahabatController = require('../../controllers/v2/SahabatOdha');
var reminderController = require('../../controllers/v2/Reminder');
var userController = require('../../controllers/v2/user');
const faker = require('faker');

router.post('/decrypt', userController.decryptUser);

/* GET topik page. */
router.get('/topik', topikController.tes);

/* Sahabat Odha. */
router.post('/getkonsultasi',chatController.getKonsultasiFromFirebase);
router.get('/sahabatOdha',sahabatController.list);
router.get('/odha',sahabatController.listOdha);

/* Konsultasi */
router.put('/registrationToken', chatController.updateRegistrationToken);
router.post('/konsultasi', chatController.konsultasi);
router.get('/konsultasi/sahabatodha/:id', chatController.sahabatodhaListChatKonsultasi);
router.post('/konsultasi/sendMessage', chatController.konsultasiSendMessage);
router.post('/konsultasi/user', chatController.konsultasiGetMessage);

/* User. */
router.put('/user/foto',userController.editUserFoto);
router.put('/user',userController.editUser);
router.get('/user',userController.user);
router.get('/user/:iduser',userController.getOneUser);
router.post('/login',userController.loginv2);
router.post('/loginv2',userController.loginv2);
router.post('/register',userController.register);

/* Reminder */
router.get('/obat/page/:page?',reminderController.listObat);
router.post('/obat',reminderController.saveObat);
router.get('/jenisobat',reminderController.listJeniObat);
router.post('/reminder',reminderController.setReminder);
router.put('/reminder',reminderController.editReminder);
router.delete('/reminder',reminderController.deleteReminder);
router.get('/reminder/user/:id_user',reminderController.getUserReminder);
router.post('/reminder/minum',reminderController.minumObat);

module.exports = router;
