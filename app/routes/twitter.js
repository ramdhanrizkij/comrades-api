var twitter = require('../controllers/twitter');

module.exports = {
  configure: function(app) {
    app.route('/sentiment').get(twitter.sentimenbak);
    app.route('/sentiment/:page').get(twitter.sentimen);
    app.route('/sentiment/en/page/:page').get(twitter.ambil_eng2);
    app.route('/sentiment/id/page/:page').get(twitter.sentimen);

    //admin
    app.route('/sentimentMentah/id').get(twitter.listtweetsMentahId);
    app.route('/sentimentSelesai/id').get(twitter.listtweetsSelesaiId);
    app.route('/sentimentVerifikasi/id').post(twitter.VerifikasiTweetId);
    app.route('/sentimentUnVerifikasi/id').post(twitter.UnVerifikasiTweetId);

    //app.route('/testing').get(twitter.ambil_eng2);

    app.route('/ambiltweet').get(twitter.ambiltweet);
  }
};
