var arv = require('../controllers/arv');

module.exports = {
  configure: function(app) {
    app.route('/arv/jenis').get(arv.jenis);
    app.route('/arv/obat').get(arv.obat);
    app.route('/arv/reminder').post(arv.reminder);
  }
};
