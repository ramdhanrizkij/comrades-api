define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "D__comrade_dokumentasi_comrades_apidoc_main_js",
    "groupTitle": "D__comrade_dokumentasi_comrades_apidoc_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./comradeapi/public/apidoc/main.js",
    "group": "D__comrade_dokumentasi_comrades_comradeapi_public_apidoc_main_js",
    "groupTitle": "D__comrade_dokumentasi_comrades_comradeapi_public_apidoc_main_js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/event/:tipe/:lang/page/:page",
    "title": "Get Event",
    "name": "Information_Event",
    "group": "Information",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Tipe",
            "description": "<p>Event (Public/Private).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lang",
            "description": "<p>Bahasa Posting (id/en).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Pagination.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status Respon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "result",
            "description": "<p>Data hasil respone dalam bentuk array.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result._id",
            "description": "<p>id event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.nama",
            "description": "<p>Nama event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.pengirim",
            "description": "<p>Pengirim event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.tempat",
            "description": "<p>Tempat berlangsungnya event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.deskripsi",
            "description": "<p>Deskripsi event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.status",
            "description": "<p>Status event (1 artinya sudah terbit).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.tgl_posting",
            "description": "<p>Tanggal posting event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.tgl_mulai",
            "description": "<p>Tanggal mulai event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.tgl_berakhir",
            "description": "<p>Tanggal berakhir event.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.latitude",
            "description": "<p>Posisi latitude tempat event berlangsung.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.longitude",
            "description": "<p>Posisi latitude tempat event berlangsung.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.type",
            "description": "<p>type event private/public.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.kontak_person",
            "description": "<p>Kontak person yang dapat dihubungi pada event tsb.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.lang",
            "description": "<p>Bahasa (id/en).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.foto",
            "description": "<p>Foto event / banner (/pic_event/:foto).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.slug",
            "description": "<p>Slug.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n\"status\": 200,\n\"result\": [\n  {\n   \"_id\": \"586f4364409caa423cfe2448\",\n   \"id_event\": 101,\n   \"pengirim\": \"Rizal Yogi\",\n   \"slug\": \"\",\n   \"nama\": \"Tes HIV dengan Melakukan VCT\",\n   \"tempat\": \"Car Free Day Dago, Bandung\",\n   \"deskripsi\": \"<p><span style=\\\"color: rgb(75, 79, 86); font-family: \\\" san=\\\"\\\" francisco\\\",=\\\"\\\" -apple-system,=\\\"\\\" blinkmacsystemfont,=\\\"\\\" \\\".sfnstext-regular\\\",=\\\"\\\" sans-serif;=\\\"\\\" font-size:=\\\"\\\" 12px;=\\\"\\\" letter-spacing:=\\\"\\\" -0.24px;=\\\"\\\" line-height:=\\\"\\\" 15.36px;=\\\"\\\" white-space:=\\\"\\\" pre-wrap;=\\\"\\\" background-color:=\\\"\\\" rgb(241,=\\\"\\\" 240,=\\\"\\\" 240);\\\"=\\\"\\\">Pada waktu yang sudah ditentukan, akan dilakukan tes HIV untuk mengetahui\\r\\nstatus seseorang mengetai keterjangkitannya terhadap HIV/AIDS, lebih cepat\\r\\nmengetahui, lebih cepat pula untuk diatasi.</span><br></p>\",\n   \"foto\": \"1479055663494-tesVCT.jpg\",\n   \"status\": \"1\",\n   \"tgl_posting\": \"2016-05-27 11:18:21\",\n   \"tgl_mulai\": \"06 November 2016 07:00\",\n   \"tgl_berakhir\": \"07 November 2016 10:00\",\n   \"longitude\": 107.616179,\n   \"latitude\": -6.881095,\n   \"kontak_person\": \"081223518212 (Amirudin)\",\n   \"lang\": \"id\",\n   \"type\": \"public\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./informasi.js",
    "groupTitle": "Information"
  },
  {
    "type": "get",
    "url": "/posting/kategori/:kategori/:lang/page/:page",
    "title": "Get Posting",
    "name": "Information_Posting",
    "group": "Information",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "kategori",
            "description": "<p>Posting (Artikel/Berita).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lang",
            "description": "<p>Bahasa Posting (id/en).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Pagination.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status Respon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "result",
            "description": "<p>Data hasil respone dalam bentuk array.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result._id",
            "description": "<p>id posting.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.pengirim",
            "description": "<p>Nama pengirim.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.judul",
            "description": "<p>Judul Posting.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.isi",
            "description": "<p>isi Posting.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.deskripsi",
            "description": "<p>Deskripsi Posting berisi 16 - 20 kata.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.foto",
            "description": "<p>Foto posting (/pic_posting/:foto).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.status",
            "description": "<p>Status posting (1 artinya sudah terbit).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.tgl_posting",
            "description": "<p>Tanggal posting.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.kategori",
            "description": "<p>Kategori posting (Artikel/Berita).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.sumber",
            "description": "<p>Sumber posting.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.lang",
            "description": "<p>Bahasa posting (id = indonesia, en = english).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.slug",
            "description": "<p>Slug.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n\"status\": 200,\n\"result\": [\n  {\n   \"_id\": \"5abb0efa22346e1e7ca2378e\",\n   \"pengirim\": \"Rizal Yogi\",\n   \"judul\": \"AIDS dan Kisah Para Penyanyi Dunia\",\n   \"isi\": \"Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet \",\n   \"deskripsi\": \"Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet \",\n   \"foto\": '1522208506009-Annie-Lennox2_640x345_acf_cropped-1.jpg',\n   \"status\": \"1\",\n   \"tgl_posting\": \"2018-03-28 03:46:42\",\n   \"kategori\": \"Artikel\",\n   \"sumber\": \"http://rumahcemara.or.id/aids-dan-kisah-para-penyanyi-dunia/\",\n   \"lang\": \"id\",\n   \"slug\": \"http://comrade-app.azurewebsites.net/2018/aids-dan-kisah-para-penyanyi-dunia/5abb0efa22346e1e7ca2378e\",\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./informasi.js",
    "groupTitle": "Information"
  },
  {
    "type": "get",
    "url": "/v2/topik",
    "title": "Get Topik",
    "name": "Information_Topik",
    "group": "Information",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status Respon.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "result",
            "description": "<p>Data hasil respone dalam bentuk array.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n\"status\": 200,\n\"result\": [\n  {\n   \"id\": 1,\n   \"topik\": \"Mitos HIV\"\n  },\n  {\n   \"id\": 2,\n   \"topik\": \"Berita\"\n  },\n  {\n   \"id\": 3,\n   \"topik\": \"Konsultasi\"\n  },\n  {\n   \"id\": 4,\n   \"topik\": \"Komunitas Berbagi\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./informasi.js",
    "groupTitle": "Information"
  },
  {
    "type": "get",
    "url": "/v2/jenisobat",
    "title": "Get Jenis Obat",
    "name": "Jenis_Obat",
    "group": "Obat",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n \"status\": 200,\n \"message\": \"success\",\n \"result\": [\n     {\n        \"id_jenis\": 0,\n        \"nama\": null,\n        \"note\": null\n     },\n     {\n        \"id_jenis\": 4,\n        \"nama\": \"Entry Inhibitor\",\n        \"note\": \"Jenis ini menghambat virus HIV agar tidak bisa masuk ke dalam sel CD4 (langkah 2 diatas)\"\n     },\n     {\n        \"id_jenis\": 14,\n        \"nama\": \"Nucleoside Reverse Transcriptase Inhibit\",\n        \"note\": \"Jenis ini menghambat langkah ketiga diatas dimana RNA virus tidak akan bisa dirubah menjadi DNA virus\"\n     }\n }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Obat"
  },
  {
    "type": "get",
    "url": "/v2/obat/page/:page",
    "title": "Get Obat",
    "name": "Obat",
    "group": "Obat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "page",
            "description": "<p>Optional page for pagination, it will return all of result obat if not filled and the limit is 8 data.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n  \"status\": 200,\n  \"message\": \"Success\",\n  \"result\": {\n       {\n        \"id_obat\": 4,\n        \"merek\": \"ABAC\",\n        \"produsen\": \"Ranbaxy\",\n        \"kandungan\": \"Abacavir 300mg\",\n        \"bentuk\": \"Tablet\",\n        \"jenis_obat\": {\n            \"id_jenis\": 14,\n            \"nama\": \"Nucleoside Reverse Transcriptase Inhibit\",\n            \"note\": \"Jenis ini menghambat langkah ketiga diatas dimana RNA virus tidak akan bisa dirubah menjadi DNA virus\"\n        }\n      },\n      {\n        \"id_obat\": 14,\n        \"merek\": \"ABAC-LZ\",\n        \"produsen\": \"Ranbaxy\",\n        \"kandungan\": \"Abacavir 300mg + Lamivudine 150mg + Zidovudine 300mg\",\n        \"bentuk\": \"Tablet\",\n        \"jenis_obat\": {\n            \"id_jenis\": 14,\n            \"nama\": \"Nucleoside Reverse Transcriptase Inhibit\",\n            \"note\": \"Jenis ini menghambat langkah ketiga diatas dimana RNA virus tidak akan bisa dirubah menjadi DNA virus\"\n        }\n      }\n }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Obat"
  },
  {
    "type": "post",
    "url": "/v2/obat/page",
    "title": "Save Obat",
    "name": "Save_Obat",
    "group": "Obat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_jenis_obat",
            "description": "<p>Id Jenis Obat, set 0 if there is no jenis_obat that match.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "merek",
            "description": "<p>merek/nama Obat.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "produsen",
            "description": "<p>Produsen Obat.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "kandungan",
            "description": "<p>Kandungan Obat.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "bentuk",
            "description": "<p>Bentuk obat , ex. tablet, sirop, kapsul</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n \"status\": 200,\n \"message\": \"success\",\n \"result\": {\n    \"id_obat\": 595,\n    \"id_jenis_obat\": \"0\",\n    \"merek\": \"Loremipsum\",\n    \"produsen\": \"Lorem\",\n    \"kandungan\": \"Ipsum\",\n    \"bentuk\": \"dolor\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n \"status\": 400,\n \"message\": \"Validation error: Validation notEmpty failed\",\n \"result\": [\n     {\n         \"message\": \"Validation notEmpty failed\",\n         \"type\": \"Validation error\",\n         \"path\": \"merek\",\n         \"value\": {},\n         \"__raw\": {}\n     }\n ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Obat"
  },
  {
    "type": "put",
    "url": "/v2/user",
    "title": "Ubah Data User",
    "name": "Ubah_Data_User",
    "group": "Profil",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "nama",
            "description": "<p>Nama User</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "tgl_lahir",
            "description": "<p>Tanggal Lahir Format DD-MM-YYYY</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "telp",
            "description": "<p>Nomor telepon User</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "jenis_kelamin",
            "description": "<p>Jenis Kelamin (L/P)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "id_user",
            "description": "<p>ID User that want to change the data</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n \"status\": 200,\n \"message\": \"Success\",\n \"result\": {\n     \"id_user\": 1706,\n     \"nama\": \"asdasd\",\n     \"email\": \"cb1205c0c1b99c9b91376c7f2c91cc41a673645a4db4ef956dbda153aa053505\",\n     \"jk\": \"P\",\n     \"telp\": \"0812312212\",\n     \"tgl_lahir\": \"19-02-1997\",\n     \"foto\": \"default.png\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n \"status\": 400,\n \"message\": \"failed\",\n \"result\": {\n     \"name\": \"SequelizeValidationError\",\n     \"message\": \"Validation error: Validation max failed\",\n     \"errors\": [\n         {\n             \"message\": \"Validation max failed\",\n             \"type\": \"Validation error\",\n             \"path\": \"telp\",\n             \"value\": {},\n             \"__raw\": {}\n         }\n     ]\n }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "Profil"
  },
  {
    "type": "put",
    "url": "/v2/user/foto",
    "title": "Ubah Foto User",
    "name": "Ubah_Foto_User",
    "group": "Profil",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "foto",
            "description": "<p>Foto User.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_user",
            "description": "<p>ID user.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n \"status\": 200,\n \"message\": \"Success\",\n \"result\": {\n     \"id_user\": 1705,\n     \"foto\": \"1534945567531-Warranty Status.JPG\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n   \"status\": 400,\n   \"message\": \"failed\",\n   \"result\": \"Foto not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "Profil"
  },
  {
    "type": "put",
    "url": "/v2/reminder",
    "title": "Edit Reminder",
    "name": "Edit_Reminder",
    "group": "Reminder",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_obat",
            "description": "<p>ID Obat.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_reminder",
            "description": "<p>ID Reminder.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "waktu_reminder",
            "description": "<p>Waktu Reminder, value : 8/12 (jam).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "jam_mulai",
            "description": "<p>Jam Mulai Remindeer , ex.12:01:12 .</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n   \"status\": 200,\n   \"message\": \"success\",\n   \"result\": [\n       1\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n   \"status\": 400,\n   \"message\": \"Validation error: Validation notEmpty failed,\\nValidation error: Validation isIn failed\",\n   \"result\": [\n       {\n           \"message\": \"Validation notEmpty failed\",\n           \"type\": \"Validation error\",\n           \"path\": \"id_obat\",\n           \"value\": {},\n           \"__raw\": {}\n       },\n       {\n           \"message\": \"Validation isIn failed\",\n           \"type\": \"Validation error\",\n           \"path\": \"waktu_reminder\",\n           \"value\": {},\n           \"__raw\": {}\n       }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Reminder"
  },
  {
    "type": "delete",
    "url": "/v2/reminder",
    "title": "Hapus Reminder",
    "name": "Hapus_Reminder",
    "group": "Reminder",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_reminder",
            "description": "<p>ID Reminder.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n \"status\": 200,\n \"message\": \"success\",\n \"result\": []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n   \"status\": 400,\n   \"message\": \"ID Reminder not found\",\n   \"result\": []\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Reminder"
  },
  {
    "type": "post",
    "url": "/v2/reminder/minum",
    "title": "Save Minum Obat Reminder",
    "name": "Save_Minum_Obat_Reminder",
    "group": "Reminder",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_reminder",
            "description": "<p>ID Reminder.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n   \"status\": 200,\n   \"message\": \"success\",\n   \"result\": {\n       \"id_history\": 5,\n       \"id_reminder\": \"7\",\n       \"waktu_minum\": \"17:58:9\",\n       \"tanggal_minum\": \"2018-08-20T00:00:00.000Z\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n{\n \"status\": 400,\n \"message\": \"ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails (`comrades`.`reminder_history`, CONSTRAINT `fk_id_reminder` FOREIGN KEY (`id_reminder`) REFERENCES `reminderv2` (`id_reminder`))\",\n \"result\": []\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Reminder"
  },
  {
    "type": "post",
    "url": "/v2/reminder",
    "title": "Save Reminder",
    "name": "Save_Reminder",
    "group": "Reminder",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_obat",
            "description": "<p>ID Obat.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id_user",
            "description": "<p>ID User that set reminder.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "waktu_reminder",
            "description": "<p>Waktu Reminder, value : 8/12 (jam).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "jam_mulai",
            "description": "<p>Jam Mulai Remindeer , ex.12:01:12 .</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n  \"status\": 200,\n  \"message\": \"success\",\n  \"result\": {\n      \"id_reminder\": 6,\n      \"id_obat\": \"595\",\n      \"id_user\": \"1689\",\n      \"waktu_reminder\": \"8\",\n      \"jam_mulai\": \"12:01:12\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n \"status\": 400,\n \"message\": \"Validation error: Validation isIn failed\",\n \"result\": [\n     {\n         \"message\": \"Validation isIn failed\",\n         \"type\": \"Validation error\",\n         \"path\": \"waktu_reminder\",\n         \"value\": {},\n         \"__raw\": {}\n     }\n ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Reminder"
  },
  {
    "type": "get",
    "url": "/v2/reminder/user/:id_user",
    "title": "Get User Reminder",
    "name": "User_Reminder",
    "group": "Reminder",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id_user",
            "description": "<p>ID User that want to see his reminder.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n  \"status\": 200,\n  \"message\": \"success\",\n  \"result\": [\n      {\n         \"id_reminder\": 6,\n         \"id_user\": 1689,\n         \"waktu_reminder\": \"8\",\n         \"jam_mulai\": \"12:01:12\",\n         \"obat\": {\n              \"id_obat\": 595,\n              \"id_jenis_obat\": 0,\n              \"merek\": \"Loremipsum\",\n              \"produsen\": \"Lorem\",\n              \"kandungan\": \"Ipsum\",\n              \"bentuk\": \"dolor\"\n          }\n      },\n      {\n          \"id_reminder\": 7,\n          \"id_user\": 1689,\n          \"waktu_reminder\": \"12\",\n          \"jam_mulai\": \"12:01:12\",\n          \"obat\": {\n              \"id_obat\": 24,\n              \"id_jenis_obat\": 14,\n              \"merek\": \"ABAMUNE\",\n              \"produsen\": \"Cipla\",\n              \"kandungan\": \"Abacavir 300mg\",\n              \"bentuk\": \"Tablet\"\n          }\n      }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./obat.js",
    "groupTitle": "Reminder"
  },
  {
    "type": "post",
    "url": "/v2/login",
    "title": "Post Login User",
    "name": "Login_User",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "typeLogin",
            "description": "<p>Login Tipe (google/default).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "registration_token_fcm",
            "description": "<p>registration_token_fcm Required if user first time login with google</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n  \"status\": 200,\n  \"message\": \"Success\",\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXF9.GLyzQiaaZNlVWxfx1KJOg3zIf-hpolp6DH82h3Edzs4\",\n  \"result\": {\n     \"id_user\": 1391,\n     \"nama\": \"Lolox\",\n     \"email\": \"7a37c46df490c08bbd16e78cc3e4f1058d13f329b839056ed646b5d93e713d7c\",\n     \"jk\": null,\n     \"telp\": \"081355703112\",\n     \"tgl_lahir\": null,\n     \"status\": \"1\",\n     \"jenis_user\": \"Sahabat Odha\",\n     \"foto\": \"default.png\",\n     \"private_key\": \"comrade@codelabs\",\n     \"registration_token_fcm\": null,\n     \"firebase_token\": null\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 401 Unauthorize\n\n{\n  \"status\": 401,\n  \"message\": \"Authentication failed. Email not found.\",\n  \"result\": {}\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/v2/odha",
    "title": "Get Odha",
    "name": "Odha",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\n  \"status\": 200,\n  \"message\": \"success\",\n  \"result\": [\n     {\n       \"id_user\": 21,\n       \"nama\": \"Andiki Setia Day\",\n       \"email\": \"asd@yahoo.com\",\n       \"jk\": \"L\",\n       \"telp\": \"085871500098\",\n       \"tgl_lahir\": \"09-11-1980\",\n       \"status\": \"1\",\n       \"jenis_user\": \"Odha\",\n       \"foto\": \"1499409122790-IMG-20170704-WA0001.jpg\",\n       \"private_key\": \"comrade@codelabs\",\n       \"token_fcm\": null,\n       \"uid\": \"RTaEiYK31cUkBk156xUCLAttRnC2\"\n     },\n     {\n       \"id_user\": 251,\n       \"nama\": \"Rizal Y\",\n       \"email\": \"rizalyogip@gmail.com\",\n       \"jk\": \"L\",\n       \"telp\": \"081328384384\",\n       \"tgl_lahir\": \"19-11-1989\",\n       \"status\": \"0\",\n       \"jenis_user\": \"Odha\",\n       \"foto\": \"default.png\",\n       \"private_key\": \"comrade@codelabs\",\n       \"token_fcm\": null,\n       \"uid\": null\n     }\n   }\n ]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/v2/register",
    "title": "Post Register User",
    "name": "Register_User",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password user.</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "nama",
            "description": "<p>Nama User</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "tgl_lahir",
            "description": "<p>Tanggal Lahir Format DD-MM-YYYY</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "telp",
            "description": "<p>Nomor telepon User</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "jk",
            "description": "<p>Jenis Kelamin (L/P)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "registration_token_fcm",
            "description": "<p>Registration Token User</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n   \"status\": 200,\n   \"message\": \"success\",\n   \"result\": {\n       \"User\": {\n           \"id_user\": 1689,\n           \"nama\": \"d537802a62386ac4f0c0b12208ffb276\",\n           \"tgl_lahir\": \"614e53098e009dae2e613bbafa58987e\",\n           \"email\": \"64bcfbb7c08ee62d8dae546efd3323ab708089719fcd477b475c6e9624cbd226\",\n           \"jk\": \"L\",\n           \"registration_token_fcm\": \"asfasdasd\",\n           \"telp\": null,\n           \"jenis_user\": \"User\",\n           \"private_key\": \"tqx1STp4Lz\"\n       },\n       \"firebase\": {\n           \"apiKey\": \"AIzaSyCi99-xRageikz9hO2JHCcKeb1GSPKcOeU\",\n           \"refreshToken\": \"AGdpqewrgbYaqT5k4zDi0u_GzFTfUjiwh2Abw5ox8uCWiUSHgmb_K6I8jizSG6tpp6-Oy9hVNJ3rM5l1Laa1lQ\",\n           \"accessToken\": \"eyJhbGciOiJSUzI1NiIsImtpZCI6ImZmNTRmZjM0MTFiZmMx6voF0avD11pxArVFmceqJICegkHyvpg\",\n           \"expirationTime\": 1533734188436\n       }\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 400 Bad Request\n\n{\n \"status\": 400,\n \"message\": \"Validation error: Email already exist\",\n \"result\": [\n     {\n         \"message\": \"Email already exist\",\n         \"type\": \"Validation error\",\n         \"path\": \"email\",\n         \"value\": {\n             \"cause\": {},\n             \"isOperational\": true\n         },\n         \"__raw\": {\n             \"cause\": {},\n             \"isOperational\": true\n         }\n     }\n ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/v2/sahabatodha",
    "title": "Get Sahabat Odha",
    "name": "Sahabat_Odha",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n\"status\": 200,\n\"result\": [\n  {\n     \"id_user\": 331,\n     \"nama\": \"Michael Hudson\",\n     \"email\": \"296d7c56909e3a528d27412a5077f7e7a673645a4db4ef956dbda153aa053505\",\n     \"jk\": \"L\",\n     \"telp\": \"085871500098\",\n     \"tgl_lahir\": \"09-06-1988\",\n     \"status\": \"1\",\n     \"jenis_user\": \"Sahabat Odha\",\n     \"foto\": \"default.png\",\n     \"private_key\": null,\n     \"token_fcm\": null,\n     \"sahabat_odha\": {\n         \"id_sahabatodha\": 51,\n         \"id_user\": 331,\n         \"komunitas\": \"Remaja Peduli HIV/ AIDS\",\n         \"pekerjaan\": null,\n         \"institusi\": null,\n         \"usia\": null,\n         \"about_sahabatodha\": \"My name is Michael Hudson. Younger who active to do some positive activity that care about HIV/AIDS.\\r\\nI'm with my friends in my community always organize an events to educate society and to raise awareness for HIV/AIDS.\\r\\nTo be a people who active in positive community make me grow up and always learn to be more mature.\\r\\nI'm here, i'm care.\\r \",\n         \"harga\": 5000,\n         \"asal_kota\": \"Bandung\",\n         \"status_aktivasi\": \"1\"\n     },\n     \"ratings\": [\n       {\n         \"rating\": 3.75,\n         \"count\": 4\n       }\n     ]\n  },\n  {\n    \"id_user\": 1311,\n    \"nama\": \"Dadan Darmawan\",\n    \"email\": \"be9c47ad11377d9e243124eb4ea59ee88ef242470efdfe342f68158509d1660b\",\n    \"jk\": null,\n    \"telp\": \"088211175519\",\n    \"tgl_lahir\": null,\n    \"status\": \"0\",\n    \"jenis_user\": \"Sahabat Odha\",\n    \"foto\": \"default.png\",\n    \"private_key\": \"comrade@codelabs\",\n    \"registration_token_fcm\": null,\n    \"uid\": null,\n    \"sahabat_odha\": {\n        \"id_sahabatodha\": 251,\n        \"id_user\": 1311,\n        \"komunitas\": \"Rumah Cemara\",\n        \"pekerjaan\": \"Mahasiswa\",\n        \"institusi\": \"Unikom\",\n        \"usia\": 19,\n        \"about_sahabatodha\": \"Saya peduli ODHA\",\n        \"harga\": 6000,\n        \"asal_kota\": \"Bandung\",\n        \"status_aktivasi\": \"1\"\n    },\n    \"ratings\": [\n    ]\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/v2/user",
    "title": "Get User",
    "name": "User",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n\"status\": 200,\n\"result\": [\n  {\n      \"id_user\": 731,\n      \"nama\": \"Rifqi Mubaroq \",\n      \"email\": \"mubaroq@upi.edu\",\n      \"jk\": \"\",\n      \"telp\": {},\n      \"tgl_lahir\": {},\n      \"status\": \"1\",\n      \"jenis_user\": \"User\",\n      \"foto\": \"default.png\",\n      \"private_key\": \"comrade@codelabs\",\n      \"registration_token_fcm\": null,\n      \"firebase_token\": null\n  },\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/v2/konsultasi/user",
    "title": "Chat Konsultasi User",
    "name": "Get_Chat_Konsultasi",
    "group": "konsultasi",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user1",
            "description": "<p>id user 1.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user2",
            "description": "<p>id user 2.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n   \"status\": 200,\n   \"message\": \"success\",\n   \"result\": {\n       \"user1\": {\n           \"id_user\": 1711,\n           \"registration_token_fcm\": \"jhgjhjh70yuhy89asd532\",\n           \"nama\": \"8a7ca9fea33b99ad8644ae0753965552\",\n           \"foto\": \"default.png\"\n       },\n       \"user2\": {\n           \"id_user\": 1713,\n           \"registration_token_fcm\": \"jhgjhjh70yuhy89asd123\",\n           \"nama\": \"bfe78ee98bdae808db3e18ea4e7cd633\",\n           \"foto\": \"default.png\"\n       },\n       \"listChat\": [\n           {\n               \"id_chat\": 3,\n               \"registration_token_from\": \"jhgjhjh70yuhy89asd123\",\n               \"registration_token_to\": \"jhgjhjh70yuhy89asd532\",\n               \"message\": \"asd\",\n               \"time\": \"2018-09-17T03:17:45.000Z\"\n           },\n           {\n               \"id_chat\": 4,\n               \"registration_token_from\": \"jhgjhjh70yuhy89asd532\",\n               \"registration_token_to\": \"jhgjhjh70yuhy89asd123\",\n               \"message\": \"asd2\",\n               \"time\": \"2018-09-17T03:23:13.000Z\"\n           }\n       ]\n   }\n}",
          "type": "json"
        },
        {
          "title": "Not-Found-Response:",
          "content": "    HTTP/1.1 400 OK\n\n{\n   \"status\": 400,\n   \"message\": \"user not found\",\n   \"result\": {}\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./konsultasi.js",
    "groupTitle": "konsultasi"
  },
  {
    "type": "get",
    "url": "/v2/konsultasi/sahabatodha/:id",
    "title": "List Chat Konsultasi Sahabat Odha",
    "name": "Get_List_Chat_Konsultasi_Sahabat_Odha",
    "group": "konsultasi",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id user.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 400 OK\n\n{\n   \"status\": 200,\n   \"message\": \"success\",\n   \"result\": {\n       \"user\": {\n           \"id_user\": 1713,\n           \"registration_token_fcm\": \"jhgjhjh70yuhy89asd123\",\n           \"nama\": \"bfe78ee98bdae808db3e18ea4e7cd633\",\n           \"foto\": \"default.png\"\n       },\n       \"listChatSahabatOdha\": [\n           {\n               \"id_chat\": 4,\n               \"registration_token_to\": \"jhgjhjh70yuhy89asd532\",\n               \"message\": \"asd2\",\n               \"time\": \"2018-09-17T03:23:13.000Z\",\n               \"UserTo\": {\n                   \"id_user\": 1711,\n                   \"nama\": \"8a7ca9fea33b99ad8644ae0753965552\",\n                   \"email\": \"21311061026ad42acced89c2bde1d5ec\",\n                   \"jk\": \"L\",\n                   \"telp\": \"32541409c140cb2564dfc930db977eaf\",\n                   \"tgl_lahir\": \"542759e0698bb31a6094adfbc79c7816\",\n                   \"jenis_user\": \"User\",\n                   \"foto\": \"default.png\",\n                   \"registration_token_fcm\": \"jhgjhjh70yuhy89asd532\"\n               }\n           },\n           {\n               \"id_chat\": 3,\n               \"registration_token_to\": \"jhgjhjh70yuhy89asd321\",\n               \"message\": \"asd\",\n               \"time\": \"2018-09-17T03:17:45.000Z\",\n               \"UserTo\": {\n                   \"id_user\": 1712,\n                   \"nama\": \"8e76d59a95e3c6cbc571b6969a620de9\",\n                   \"email\": \"68796c3d5fccddbf29799160bd074e8a0f06d128d1fe1c5a085baa48a37a8c4f\",\n                   \"jk\": \"L\",\n                   \"telp\": \"999609784f67e4ea490c19be76edaedd\",\n                   \"tgl_lahir\": \"00f030641f5f32bd36a1551990be4577\",\n                   \"jenis_user\": \"User\",\n                   \"foto\": \"default.png\",\n                   \"registration_token_fcm\": \"jhgjhjh70yuhy89asd321\"\n               }\n           }\n       ]\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./konsultasi.js",
    "groupTitle": "konsultasi"
  },
  {
    "type": "post",
    "url": "/v2/konsultasi/sendMessage",
    "title": "Send Message Konsultasi",
    "name": "Konsultasi___Send_Message",
    "group": "konsultasi",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "registrationTokenTo",
            "description": "<p>Registration Token User yang akan menerima pesan.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "registrationTokenFrom",
            "description": "<p>Registration Token User yang akan mengirimkan pesan.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Pesan Konsultasi.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n  \"status\": 200,\n  \"message\": \"success\",\n  \"result\": \"projects/new-comrades/messages/0:1537231398065882%fff8f61ef9fd7ecd\"\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 400 OK\n\n{\n   \"status\": 400,\n   \"message\": \"Request contains an invalid argument.\",\n   \"result\": {\n       \"code\": \"messaging/invalid-argument\",\n       \"message\": \"Request contains an invalid argument.\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./konsultasi.js",
    "groupTitle": "konsultasi"
  },
  {
    "type": "put",
    "url": "/v2/registrationToken",
    "title": "Update Registration Token FCM User",
    "name": "Update_Registration_Token",
    "group": "konsultasi",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id_user",
            "description": "<p>id user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "registration_token_fcm",
            "description": "<p>Uid Registration Token yang ingin didaftarkan.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\n{\n  \"status\": 200,\n  \"message\": \"success\",\n  \"result\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./konsultasi.js",
    "groupTitle": "konsultasi"
  }
] });
