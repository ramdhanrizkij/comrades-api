/**
 * @author Tioreza Febrian
 * @email tiorezafk@outlook.com
 * @create date 2018-07-11 14:33:01
 * @modify date 2018-05-11 14:33:10
*/

'use strict';

const Sequelize = require(`sequelize`);
const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE,
  process.env.MYSQL_USERNAME,
  process.env.MYSQL_PASSWORD, {
    host: process.env.MYSQL_HOST,
    dialect:'mysql',
    port: process.env.MYSQL_PORT,

    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },

    logging: false,
  });

sequelize.authenticate().then(() => {
  console.log(`Database connection with sequelize has been successfully established`);
}).catch((err) => {
  console.log(`Database unable to connect sequelize: ${err.message}`);
});

module.exports = sequelize;
