var admin = require("firebase-admin");

var serviceAccount = require("../public/service-account.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://new-comrades.firebaseio.com"
});

module.exports = admin;
