var db = require('./db');
var moment = require('moment');
var reminder = require('./reminder');
var cron = require('cron');

var remind = new cron.CronJob({
  cronTime: '* * * * * *',
  onTick: function() {
    db.acquire(function(err,con){
      con.query(`SELECT * FROM new_arv_reminder ORDER BY id_reminder DESC`, function(err,data){
        con.release();
          if (!data.length) {
              console.log('Data Reminder Kosong');
          }else{
            // data.forEach((key,value) => {
              if(data[0].waktu_reminder == moment().tz("Asia/Jakarta").format('HH:mm:ss')) {
                // console.log(key.time);
                console.log('ya');
                // console.log(data);
                var dataHealbox = {
                  id_healbox: data[0].id_healbox,
                  id_obat: data[0].id_obat,
                  data1: data[0].data1,
                  data2: data[0].data2,
                  data3: data[0].data3
                };

                reminder.kirimReminder(dataHealbox);
              }else{
                // console.log(moment().format('HH:mm:ss'));
                console.log('Reminder : ' +data[0].waktu_reminder);
                console.log('time : '+moment().tz("Asia/Jakarta").format('HH:mm:ss'));
                console.log('no');
              }
            // });
          }
      });
    });
  },
  start: false
});

//run CronJob Reminder
//remind.start();
