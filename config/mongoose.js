var mongoose = require('mongoose');

module.exports = function() {

    const mongodbOptions = {
        useMongoClient: true,
        autoReconnect: true,
        socketTimeoutMS: 0,
        connectTimeoutMS: 0,
      };

	//var db = mongoose.connect("mongodb://comradeapp:J4znR06PrOVIEGq1VM5idwiDJeCw7PlulLIjfFztNfp41KF5Ii9jOXDPozxMD4MKRRvCWlerpRNUo3M1SqXLjA==@comradeapp.documents.azure.com:10250/comradedb?ssl=true");
	//mongoose.connect('mongodb://comradesapp:comrades2017@ds163595.mlab.com:63595/comrades', { useMongoClient: true });
	mongoose.connect('mongodb://comradesapp:comrades2018@ds163595.mlab.com:63595/comrades', mongodbOptions);
	// mongoose.connect('mongodb://cumanread:codelabs2017@52.230.4.225:27017/comrades', { useMongoClient: true });

	var db = mongoose.connection
	db.on('error', console.error.bind(console, 'connection error:'));
	db.on('open',function(){
		console.log('connected to mongo');
	});

	require('../app/models/posting');
	require('../app/models/twitter');
	require('../app/models/event');
	return db;
};
//mongoexport --host ds145365.mlab.com:45365 --type json --username admin --password admin --collection postings --db sentiment_support --out a.json
// mongoimport --host ds163595.mlab.com:63595 --type json --username comradesapp --password comrades2017 --collection events --db comrades --file backupEvent.json
// /mongoimport --host ds163595.mlab.com:63595 --type json --username comradesapp --password comrades2018 --collection postings --db comrades --file postings.json
