'use strict';

const dotenv = require('dotenv');

console.log('Node Environment', process.env.NODE_ENV);
if (process.env.NODE_ENV === 'production') {
  dotenv.config({ path: '.env.production' });
} else {
  dotenv.config({ path: '.env.development' });
}
