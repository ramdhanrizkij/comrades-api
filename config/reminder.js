var mqtt = require('mqtt');
var cron = require('cron');
var redis = require('redis');
var moment = require('moment');
var db = require('./db');

var clientRedis = redis.createClient('//redis-17092.c13.us-east-1-3.ec2.cloud.redislabs.com:17092',{
    password : "divernity2017"
});

clientRedis.on('error', function(err){
    console.log('Error ' + err);
});

var option = {
  username : 'codelabs',
  password : 'UnikomCodelabs15'
}
var client  = mqtt.connect('mqtt://52.187.21.83:1883');

client.on('connect', function () {
  console.log("client is online");
});

client.on('message', function (topic, message) {
  // message is Buffer
  console.log('Waiting for data ...');
  var data = message.toString();
  // console.log(topic)
  if(topic == '/healbox/id') {
    console.log(`Ini ID Healbox : ${data}`);
    // sendStatistik(data);
  }

  if(topic == '/healbox/suhu') {
    console.log(`Suhu : ${data}`);
  }

  if(topic == '/healbox/kelembaban') {
    console.log(`Kelembaban : ${data}`);
  }

  if(topic == '/healbox/gps') {
    console.log(`Gps : ${data}`);
  }

  if(topic == '/joko/ltd') {
    console.log(`Latitude : ${data}`);
  }

  if(topic == '/joko/lgd') {
    console.log(`Longitude : ${data}`);
  }
  // client.end()
});


/*** client on error ***/
client.on("error", function(err) {
  console.log("error from client --> ", err);
});

/*** client on close ***/
client.on("close", function() {
    console.log("client is closed");
});
  /*** client on offline ***/
client.on("offline", function(err) {
  console.log("client is offline");
});

/*** client on reconnect ***/
client.on("reconnect", function() {
  console.log("client is reconnected");
});

client.subscribe('/healbox/inTopic');
client.subscribe('/healbox/outTopic');
client.subscribe('/healbox/id');
client.subscribe('/healbox/suhu');
client.subscribe('/healbox/kelembaban');
client.subscribe('/healbox/gps');
client.subscribe('/joko/ltd');
client.subscribe('/joko/lgd');

module.exports = {

  sendDataTes : (data,done) => {

    client.publish('/healbox/inTopic', JSON.stringify(data));

    done(null,data);

  },

  setReminder : (data,done) => {
    db.acquire(function(err,con){
      con.query(`INSERT INTO new_arv_reminder SET ?`,data, function(err,datas){
        con.release();
        if(err)
          done(err,null);

        var respon = {
          status:200,
          message:'Data Reminder Berhasil Disimpan',
          result:data
        }

        done(null,respon);

      });
    });
  },

  kirimReminder : (data,done) => {

    console.log('Data Siap Kirim : ');
    console.log(data);

    client.publish('/healbox/inTopic', JSON.stringify(data));

    return true;
  }

};
